<?php defined('C5_EXECUTE') or die("Access Denied.");
$navigationTypeText = ($navigationType == 0) ? 'arrows' : 'pages';
$c = Page::getCurrentPage();
if ($c->isEditMode()) {
	?>
	<div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
		<i style="font-size:40px; margin-bottom:20px; display:block;" class="fa fa-picture-o" aria-hidden="true"></i>
		<div style="padding: 40px 0px 40px 0px"><?php echo t('Image Slider disabled in edit mode.')?>
			<div style="margin-top: 15px; font-size:9px;">
				<i class="fa fa-circle" aria-hidden="true"></i>
				<?php if (count($rows) > 0) { ?>
					<?php foreach (array_slice($rows, 1) as $row) { ?>
						<i class="fa fa-circle-thin" aria-hidden="true"></i>
						<?php }
					}
				?>
			</div>
		</div>
	</div>
<?php } else { ?>
	<?php if (count($rows) > 0) {?>
		<div class="swiper-container-mv">
			<div class="swiper-wrapper">
				<?php foreach ($rows as $row) {?>
					<?php if ($row['linkURL']) {?>
						<a href="<?php echo $row['linkURL'] ?>" class="mega-link-overlay"></a>
					<?php } ?>
					<?php 
					$f = File::getByID($row['fID']);
					if (is_object($f)) {
						echo '<div class="swiper-slide" style="background-size:cover;background-position:center center;background-image:url('.$f->getURL().');"></div>';
					}
					?>
				<?php } ?>
			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
		<script>
			$(document).ready(function(){
				 var swiper = new Swiper('.swiper-container-mv', {
				 	speed:1200,
					pagination: '.swiper-pagination',
					nextButton: '.swiper-button-next',
					prevButton: '.swiper-button-prev',
					paginationClickable: true,
					centeredSlides: true,
					followFinger: false,
					autoplay: 3500,
					autoplayDisableOnInteraction: false,
					loop:true,
					effect:'fade',
					fade: {
						crossFade: true
					}
				});
			});
		</script>
	<?php } else { ?>
		<div class="ccm-image-slider-placeholder">
			<p><?php echo t('No Slides Entered.');?></p>
		</div>
	<?php } ?>
<?php } ?>
