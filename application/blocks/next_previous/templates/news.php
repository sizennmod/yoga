<?php
defined('C5_EXECUTE') or die("Access Denied.");

if (!$previousLinkURL && !$nextLinkURL && !$parentLabel) {
    return false;
}
?>

<div class="ccm-block-next-previous-wrapper">
    <?php

    if ($previousLinkText) {
        ?>
        <p class="ccm-block-next-previous-previous-link">
            &lt;&lt;&nbsp;<?php echo $previousLinkURL ? '<a href="' . $previousLinkURL . '">' . $previousLinkText . '</a>' : '' ?>
        </p>
        <?php
    }

    

    if ($nextLinkText) {
        ?>
        <p class="ccm-block-next-previous-next-link">
            <?php echo $nextLinkURL ? '<a href="' . $nextLinkURL . '">' . $nextLinkText . '</a>' : '' ?>&nbsp;&gt;&gt;
        </p>
        <?php
    }

    if ($parentLabel) {
        ?>
       <!--  <p class="ccm-block-next-previous-parent-link">
            <?php echo $parentLinkURL ? '<a href="' . $parentLinkURL . '">' . $parentLabel . '</a>' : '' ?>
        </p> -->
        <?php
    }
    ?>
</div>
