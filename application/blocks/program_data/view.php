<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($dataloop_items)) { ?>
<ul class="row list--program">

	<?php foreach ($dataloop_items as $dataloop_item_key => $dataloop_item) { ?>
		<li class="col-5">
			<?php if (trim($dataloop_item["link_URL"]) != "") { ?>
				<?php
				$dataloop_itemlink_Attributes = array();
				$dataloop_itemlink_Attributes['href'] = $dataloop_item["link_URL"];
				$dataloop_item["link_AttributesHtml"] = join(' ', array_map(function ($key) use ($dataloop_itemlink_Attributes) {
					return $key . '="' . $dataloop_itemlink_Attributes[$key] . '"';
				}, array_keys($dataloop_itemlink_Attributes)));
				echo sprintf('<a %s>', $dataloop_item["link_AttributesHtml"]); 
				?>
			<?php } ?>
				
			<?php if (isset($dataloop_item["hour"]) && trim($dataloop_item["hour"]) != "") { ?>
				<div class="text text--add">
					<p class="hour"><?php echo h($dataloop_item["hour"]); ?></p>
				</div>
			<?php } ?>
			
			<?php if ($dataloop_item["photo"]) { ?>
				<img src="<?php echo $dataloop_item["photo"]->getRecentVersion()->getThumbnailURL('thumb176_125_2x'); ?>" alt="<?php echo $dataloop_item["photo"]->getTitle(); ?>"/>
			<?php } ?>
			
				<div class="text">
				<?php if (isset($dataloop_item["ttl"]) && trim($dataloop_item["ttl"]) != "") { ?>
					<h3 class="ttl"><?php echo h($dataloop_item["ttl"]); ?></h3>
				<?php } ?>
				
				<?php if (isset($dataloop_item["category"]) && trim($dataloop_item["category"]) != "") { ?>
					<p class="category"><?php echo nl2br(h($dataloop_item["category"])); ?></p>
				<?php } ?>
				</div>
			<?php if (trim($dataloop_item["link_URL"]) != "") { echo '</a>';}?>
			
		</li>
	<?php } ?>

</ul>
<?php } ?>