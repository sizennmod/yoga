<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
<?php $repeatable_container_id = 'btProgramData-dataloop-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $dataloop_items,
                        'order' => array_keys($dataloop_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('データセット') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('dataloop'); ?>[{{id}}][photo]" class="control-label"><?php echo t("写真"); ?></label>
    <?php echo isset($btFieldsRequired['dataloop']) && in_array('photo', $btFieldsRequired['dataloop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('dataloop'); ?>[{{id}}][photo]" class="ccm-file-selector ft-image-photo-file-selector" data-file-selector-f-id="{{ photo }}"></div>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('dataloop'); ?>[{{id}}][ttl]" class="control-label"><?php echo t("タイトル"); ?></label>
    <?php echo isset($btFieldsRequired['dataloop']) && in_array('ttl', $btFieldsRequired['dataloop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('dataloop'); ?>[{{id}}][ttl]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][ttl]" class="form-control" type="text" value="{{ ttl }}" maxlength="255" />
</div>            <div class="form-group">
    <label for="<?php echo $view->field('dataloop'); ?>[{{id}}][hour]" class="control-label"><?php echo t("開催時間"); ?></label>
    <?php echo isset($btFieldsRequired['dataloop']) && in_array('hour', $btFieldsRequired['dataloop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <input name="<?php echo $view->field('dataloop'); ?>[{{id}}][hour]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][hour]" class="form-control" type="text" value="{{ hour }}" maxlength="255" />
</div>            <?php $link_ContainerID = 'btProgramData-link-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $link_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link]" class="control-label"><?php echo t("リンク先"); ?></label>
	    <?php echo isset($btFieldsRequired['dataloop']) && in_array('link', $btFieldsRequired['dataloop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $dataloopLink_options = $link_Options; ?>
                    <select name="<?php echo $view->field('dataloop'); ?>[{{id}}][link]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][link]" class="form-control ft-smart-link-type">{{#select link}}<?php foreach ($dataloopLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Title]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Title]" class="form-control" type="text" value="{{ link_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Page]" data-cID="{{link_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('dataloop'); ?>[{{id}}][link_URL]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][link_URL]" class="form-control" type="text" value="{{ link_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Relative_URL]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Relative_URL]" class="form-control" type="text" value="{{ link_Relative_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="file">
		    <label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('dataloop'); ?>[{{id}}][link_File]" class="ccm-file-selector" data-file-selector-f-id="{{ link_File }}"></div>	
		</div>

		<div class="form-group hidden" data-link-type="image">
			<label for="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('dataloop'); ?>[{{id}}][link_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ link_Image }}"></div>
		</div>
		</div>
	</div>
</div>
            <div class="form-group">
    <label for="<?php echo $view->field('dataloop'); ?>[{{id}}][category]" class="control-label"><?php echo t("カテゴリー"); ?></label>
    <?php echo isset($btFieldsRequired['dataloop']) && in_array('category', $btFieldsRequired['dataloop']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('dataloop'); ?>[{{id}}][category]" id="<?php echo $view->field('dataloop'); ?>[{{id}}][category]" class="form-control" rows="5">{{ category }}</textarea>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btProgramData.dataloop.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>