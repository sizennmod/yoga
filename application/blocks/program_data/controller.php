<?php namespace Application\Block\ProgramData;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use File;
use Page;
use Permissions;
use URL;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('dataloop' => array());
    protected $btExportFileColumns = array('photo');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btProgramData', 'btProgramDataDataloopEntries');
    protected $btTable = 'btProgramData';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("プログラム");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $dataloop_items = $db->fetchAll('SELECT * FROM btProgramDataDataloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($dataloop_items as $dataloop_item_k => $dataloop_item_v) {
            if (isset($dataloop_item_v["ttl"]) && trim($dataloop_item_v["ttl"]) != "") {
                $content[] = $dataloop_item_v["ttl"];
            }
            if (isset($dataloop_item_v["hour"]) && trim($dataloop_item_v["hour"]) != "") {
                $content[] = $dataloop_item_v["hour"];
            }
            if (isset($dataloop_item_v["category"]) && trim($dataloop_item_v["category"]) != "") {
                $content[] = $dataloop_item_v["category"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $dataloop = array();
        $dataloop_items = $db->fetchAll('SELECT * FROM btProgramDataDataloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($dataloop_items as $dataloop_item_k => &$dataloop_item_v) {
            if (isset($dataloop_item_v['photo']) && trim($dataloop_item_v['photo']) != "" && ($f = File::getByID($dataloop_item_v['photo'])) && is_object($f)) {
                $dataloop_item_v['photo'] = $f;
            } else {
                $dataloop_item_v['photo'] = false;
            }
            $dataloop_item_v["link_Object"] = null;
			$dataloop_item_v["link_Title"] = trim($dataloop_item_v["link_Title"]);
			if (isset($dataloop_item_v["link"]) && trim($dataloop_item_v["link"]) != '') {
				switch ($dataloop_item_v["link"]) {
					case 'page':
						if (!empty($dataloop_item_v["link_Page"]) && ($dataloop_item_v["link_Page_c"] = Page::getByID($dataloop_item_v["link_Page"])) && !$dataloop_item_v["link_Page_c"]->error && !$dataloop_item_v["link_Page_c"]->isInTrash()) {
							$dataloop_item_v["link_Object"] = $dataloop_item_v["link_Page_c"];
							$dataloop_item_v["link_URL"] = $dataloop_item_v["link_Page_c"]->getCollectionLink();
							if ($dataloop_item_v["link_Title"] == '') {
								$dataloop_item_v["link_Title"] = $dataloop_item_v["link_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$dataloop_item_v["link_File_id"] = (int)$dataloop_item_v["link_File"];
						if ($dataloop_item_v["link_File_id"] > 0 && ($dataloop_item_v["link_File_object"] = File::getByID($dataloop_item_v["link_File_id"])) && is_object($dataloop_item_v["link_File_object"])) {
							$fp = new Permissions($dataloop_item_v["link_File_object"]);
							if ($fp->canViewFile()) {
								$dataloop_item_v["link_Object"] = $dataloop_item_v["link_File_object"];
								$dataloop_item_v["link_URL"] = $dataloop_item_v["link_File_object"]->getRelativePath();
								if ($dataloop_item_v["link_Title"] == '') {
									$dataloop_item_v["link_Title"] = $dataloop_item_v["link_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($dataloop_item_v["link_Title"] == '') {
							$dataloop_item_v["link_Title"] = $dataloop_item_v["link_URL"];
						}
						break;
				    case 'relative_url':
						if ($dataloop_item_v["link_Title"] == '') {
							$dataloop_item_v["link_Title"] = $dataloop_item_v["link_Relative_URL"];
						}
						$dataloop_item_v["link_URL"] = $dataloop_item_v["link_Relative_URL"];
						break;
				    case 'image':
						if ($dataloop_item_v["link_Image"] && ($dataloop_item_v["link_Image_object"] = File::getByID($dataloop_item_v["link_Image"])) && is_object($dataloop_item_v["link_Image_object"])) {
							$dataloop_item_v["link_URL"] = $dataloop_item_v["link_Image_object"]->getURL();
							$dataloop_item_v["link_Object"] = $dataloop_item_v["link_Image_object"];
							if ($dataloop_item_v["link_Title"] == '') {
								$dataloop_item_v["link_Title"] = $dataloop_item_v["link_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('dataloop_items', $dataloop_items);
        $this->set('dataloop', $dataloop);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btProgramDataDataloopEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $dataloop_items = $db->fetchAll('SELECT * FROM btProgramDataDataloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($dataloop_items as $dataloop_item) {
            unset($dataloop_item['id']);
            $dataloop_item['bID'] = $newBID;
            $db->insert('btProgramDataDataloopEntries', $dataloop_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $dataloop = $this->get('dataloop');
        $this->set('dataloop_items', array());
        $this->set('dataloop', $dataloop);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $dataloop = $this->get('dataloop');
        $dataloop_items = $db->fetchAll('SELECT * FROM btProgramDataDataloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($dataloop_items as &$dataloop_item) {
            if (!File::getByID($dataloop_item['photo'])) {
                unset($dataloop_item['photo']);
            }
        }
        $this->set('dataloop', $dataloop);
        $this->set('dataloop_items', $dataloop_items);
    }

    protected function addEdit()
    {
        $dataloop = array();
        $this->set("link_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('dataloop', $dataloop);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/program_data/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/program_data/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/program_data/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btProgramDataDataloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $dataloop_items = isset($args['dataloop']) && is_array($args['dataloop']) ? $args['dataloop'] : array();
        $queries = array();
        if (!empty($dataloop_items)) {
            $i = 0;
            foreach ($dataloop_items as $dataloop_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($dataloop_item['photo']) && trim($dataloop_item['photo']) != '') {
                    $data['photo'] = trim($dataloop_item['photo']);
                } else {
                    $data['photo'] = null;
                }
                if (isset($dataloop_item['ttl']) && trim($dataloop_item['ttl']) != '') {
                    $data['ttl'] = trim($dataloop_item['ttl']);
                } else {
                    $data['ttl'] = null;
                }
                if (isset($dataloop_item['hour']) && trim($dataloop_item['hour']) != '') {
                    $data['hour'] = trim($dataloop_item['hour']);
                } else {
                    $data['hour'] = null;
                }
                if (isset($dataloop_item['link']) && trim($dataloop_item['link']) != '') {
					$data['link_Title'] = $dataloop_item['link_Title'];
					$data['link'] = $dataloop_item['link'];
					switch ($dataloop_item['link']) {
						case 'page':
							$data['link_Page'] = $dataloop_item['link_Page'];
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'file':
							$data['link_File'] = $dataloop_item['link_File'];
							$data['link_Page'] = '';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'url':
							$data['link_URL'] = $dataloop_item['link_URL'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'relative_url':
							$data['link_Relative_URL'] = $dataloop_item['link_Relative_URL'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'image':
							$data['link_Image'] = $dataloop_item['link_Image'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							break;
                        default:
							$data['link'] = '';
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;	
					}
				}
				else {
					$data['link'] = '';
					$data['link_Title'] = '';
					$data['link_Page'] = '';
					$data['link_File'] = '0';
					$data['link_URL'] = '';
					$data['link_Relative_URL'] = '';
					$data['link_Image'] = '0';
				}
                if (isset($dataloop_item['category']) && trim($dataloop_item['category']) != '') {
                    $data['category'] = trim($dataloop_item['category']);
                } else {
                    $data['category'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btProgramDataDataloopEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btProgramDataDataloopEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btProgramDataDataloopEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $dataloopEntriesMin = 0;
        $dataloopEntriesMax = 0;
        $dataloopEntriesErrors = 0;
        $dataloop = array();
        if (isset($args['dataloop']) && is_array($args['dataloop']) && !empty($args['dataloop'])) {
            if ($dataloopEntriesMin >= 1 && count($args['dataloop']) < $dataloopEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("データセット"), $dataloopEntriesMin, count($args['dataloop'])));
                $dataloopEntriesErrors++;
            }
            if ($dataloopEntriesMax >= 1 && count($args['dataloop']) > $dataloopEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("データセット"), $dataloopEntriesMax, count($args['dataloop'])));
                $dataloopEntriesErrors++;
            }
            if ($dataloopEntriesErrors == 0) {
                foreach ($args['dataloop'] as $dataloop_k => $dataloop_v) {
                    if (is_array($dataloop_v)) {
                        if (in_array("photo", $this->btFieldsRequired['dataloop']) && (!isset($dataloop_v['photo']) || trim($dataloop_v['photo']) == "" || !is_object(File::getByID($dataloop_v['photo'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("写真"), t("データセット"), $dataloop_k));
                        }
                        if (in_array("ttl", $this->btFieldsRequired['dataloop']) && (!isset($dataloop_v['ttl']) || trim($dataloop_v['ttl']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タイトル"), t("データセット"), $dataloop_k));
                        }
                        if (in_array("hour", $this->btFieldsRequired['dataloop']) && (!isset($dataloop_v['hour']) || trim($dataloop_v['hour']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("開催時間"), t("データセット"), $dataloop_k));
                        }
                        if ((in_array("link", $this->btFieldsRequired['dataloop']) && (!isset($dataloop_v['link']) || trim($dataloop_v['link']) == "")) || (isset($dataloop_v['link']) && trim($dataloop_v['link']) != "" && !array_key_exists($dataloop_v['link'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク先")));
						} elseif (array_key_exists($dataloop_v['link'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($dataloop_v['link']) {
								case 'page':
									if (!isset($dataloop_v['link_Page']) || trim($dataloop_v['link_Page']) == "" || $dataloop_v['link_Page'] == "0" || (($page = Page::getByID($dataloop_v['link_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク先"), t("データセット"), $dataloop_k));
									}
									break;
				                case 'file':
									if (!isset($dataloop_v['link_File']) || trim($dataloop_v['link_File']) == "" || !is_object(File::getByID($dataloop_v['link_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク先"), t("データセット"), $dataloop_k));
									}
									break;
				                case 'url':
									if (!isset($dataloop_v['link_URL']) || trim($dataloop_v['link_URL']) == "" || !filter_var($dataloop_v['link_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク先"), t("データセット"), $dataloop_k));
									}
									break;
				                case 'relative_url':
									if (!isset($dataloop_v['link_Relative_URL']) || trim($dataloop_v['link_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク先"), t("データセット"), $dataloop_k));
									}
									break;
				                case 'image':
									if (!isset($dataloop_v['link_Image']) || trim($dataloop_v['link_Image']) == "" || !is_object(File::getByID($dataloop_v['link_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク先"), t("データセット"), $dataloop_k));
									}
									break;	
							}
						}
                        if (in_array("category", $this->btFieldsRequired['dataloop']) && (!isset($dataloop_v['category']) || trim($dataloop_v['category']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("カテゴリー"), t("データセット"), $dataloop_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('データセット'), $dataloop_k));
                    }
                }
            }
        } else {
            if ($dataloopEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("データセット"), $dataloopEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-program_data', 'blocks/program_data/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-program_data');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}