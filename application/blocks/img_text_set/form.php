<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor')?>";
</script>
            <?php
	$core_editor = Core::make('editor');
	if (method_exists($core_editor, 'getEditorInitJSFunction')) {
		/* @var $core_editor \Concrete\Core\Editor\CkeditorEditor */
		?>
		<script type="text/javascript">var blockDesignerEditor = <?php echo $core_editor->getEditorInitJSFunction(); ?>;</script>
	<?php
	} else {
	/* @var $core_editor \Concrete\Core\Editor\RedactorEditor */
	?>
		<script type="text/javascript">
			var blockDesignerEditor = function (identifier) {$(identifier).redactor(<?php echo json_encode(array('plugins' => array("concrete5lightbox", "undoredo", "specialcharacters", "table", "concrete5magic"), 'minHeight' => 300,'concrete5' => array('filemanager' => $core_editor->allowFileManager(), 'sitemap' => $core_editor->allowSitemap()))); ?>).on('remove', function () {$(this).redactor('core.destroy');});};
		</script>
		<?php
	} ?><?php $repeatable_container_id = 'btImgTextSet-repeater-container-' . $identifier_getString; ?>
    <div id="<?php echo $repeatable_container_id; ?>">
        <div class="sortable-items-wrapper">
            <a href="#" class="btn btn-primary add-entry">
                <?php echo t('Add Entry'); ?>
            </a>

            <div class="sortable-items" data-attr-content="<?php echo htmlspecialchars(
                json_encode(
                    array(
                        'items' => $repeater_items,
                        'order' => array_keys($repeater_items),
                    )
                )
            ); ?>">
            </div>

            <a href="#" class="btn btn-primary add-entry add-entry-last">
                <?php echo t('Add Entry'); ?>
            </a>
        </div>

        <script class="repeatableTemplate" type="text/x-handlebars-template">
            <div class="sortable-item" data-id="{{id}}">
                <div class="sortable-item-title">
                    <span class="sortable-item-title-default">
                        <?php echo t('データグループ') . ' ' . t("row") . ' <span>#{{id}}</span>'; ?>
                    </span>
                    <span class="sortable-item-title-generated"></span>
                </div>

                <div class="sortable-item-inner">            <div class="form-group">
    <label for="<?php echo $view->field('repeater'); ?>[{{id}}][ttl]" class="control-label"><?php echo t("タイトル"); ?></label>
    <?php echo isset($btFieldsRequired['repeater']) && in_array('ttl', $btFieldsRequired['repeater']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('repeater'); ?>[{{id}}][ttl]" id="<?php echo $view->field('repeater'); ?>[{{id}}][ttl]" class="form-control" rows="5">{{ ttl }}</textarea>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('repeater'); ?>[{{id}}][ttlStyle]" class="control-label"><?php echo t("タイトルスタイル") . ' <i class="fa fa-question-circle launch-tooltip" data-original-title="' . t("タイトルのスタイルを変更できます。") . '"></i>'; ?></label>
    <?php echo isset($btFieldsRequired['repeater']) && in_array('ttlStyle', $btFieldsRequired['repeater']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php $repeaterTtlStyle_options = $repeater['ttlStyle_options']; ?>
                    <select name="<?php echo $view->field('repeater'); ?>[{{id}}][ttlStyle]" id="<?php echo $view->field('repeater'); ?>[{{id}}][ttlStyle]" class="form-control">{{#select ttlStyle}}<?php foreach ($repeaterTtlStyle_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
</div>            <div class="form-group">
    <label for="<?php echo $view->field('repeater'); ?>[{{id}}][content]" class="control-label"><?php echo t("本文"); ?></label>
    <?php echo isset($btFieldsRequired['repeater']) && in_array('content', $btFieldsRequired['repeater']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <textarea name="<?php echo $view->field('repeater'); ?>[{{id}}][content]" id="<?php echo $view->field('repeater'); ?>[{{id}}][content]" class="ft-repeater-content">{{ content }}</textarea>
</div>            <?php $link_ContainerID = 'btImgTextSet-link-container-' . $identifier_getString; ?>
<div class="ft-smart-link" id="<?php echo $link_ContainerID; ?>">
	<div class="form-group">
		<label for="<?php echo $view->field('repeater'); ?>[{{id}}][link]" class="control-label"><?php echo t("リンク"); ?></label>
	    <?php echo isset($btFieldsRequired['repeater']) && in_array('link', $btFieldsRequired['repeater']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
	    <?php $repeaterLink_options = $link_Options; ?>
                    <select name="<?php echo $view->field('repeater'); ?>[{{id}}][link]" id="<?php echo $view->field('repeater'); ?>[{{id}}][link]" class="form-control ft-smart-link-type">{{#select link}}<?php foreach ($repeaterLink_options as $k => $v) {
                        echo "<option value='" . $k . "'>" . $v . "</option>";
                     } ?>{{/select}}</select>
	</div>
	
	<div class="form-group">
		<div class="ft-smart-link-options hidden" style="padding-left: 10px;">
			<div class="form-group">
				<label for="<?php echo $view->field('repeater'); ?>[{{id}}][link_Title]" class="control-label"><?php echo t("Title"); ?></label>
			    <input name="<?php echo $view->field('repeater'); ?>[{{id}}][link_Title]" id="<?php echo $view->field('repeater'); ?>[{{id}}][link_Title]" class="form-control" type="text" value="{{ link_Title }}" />		
			</div>
			
			<div class="form-group hidden" data-link-type="page">
			<label for="<?php echo $view->field('repeater'); ?>[{{id}}][link_Page]" class="control-label"><?php echo t("Page"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-page-selector="{{token}}" data-input-name="<?php echo $view->field('repeater'); ?>[{{id}}][link_Page]" data-cID="{{link_Page}}"></div>
		</div>

		<div class="form-group hidden" data-link-type="url">
			<label for="<?php echo $view->field('repeater'); ?>[{{id}}][link_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('repeater'); ?>[{{id}}][link_URL]" id="<?php echo $view->field('repeater'); ?>[{{id}}][link_URL]" class="form-control" type="text" value="{{ link_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="relative_url">
			<label for="<?php echo $view->field('repeater'); ?>[{{id}}][link_Relative_URL]" class="control-label"><?php echo t("URL"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <input name="<?php echo $view->field('repeater'); ?>[{{id}}][link_Relative_URL]" id="<?php echo $view->field('repeater'); ?>[{{id}}][link_Relative_URL]" class="form-control" type="text" value="{{ link_Relative_URL }}" />
		</div>

		<div class="form-group hidden" data-link-type="file">
		    <label for="<?php echo $view->field('repeater'); ?>[{{id}}][link_File]" class="control-label"><?php echo t("File"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('repeater'); ?>[{{id}}][link_File]" class="ccm-file-selector" data-file-selector-f-id="{{ link_File }}"></div>	
		</div>

		<div class="form-group hidden" data-link-type="image">
			<label for="<?php echo $view->field('repeater'); ?>[{{id}}][link_Image]" class="control-label"><?php echo t("Image"); ?></label>
            <small class="required"><?php echo t('Required'); ?></small>
            <div data-file-selector-input-name="<?php echo $view->field('repeater'); ?>[{{id}}][link_Image]" class="ccm-file-selector" data-file-selector-f-id="{{ link_Image }}"></div>
		</div>
		</div>
	</div>
</div>
            <div class="form-group">
    <label for="<?php echo $view->field('repeater'); ?>[{{id}}][img]" class="control-label"><?php echo t("画像"); ?></label>
    <?php echo isset($btFieldsRequired['repeater']) && in_array('img', $btFieldsRequired['repeater']) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <div data-file-selector-input-name="<?php echo $view->field('repeater'); ?>[{{id}}][img]" class="ccm-file-selector ft-image-img-file-selector" data-file-selector-f-id="{{ img }}"></div>
</div></div>

                <span class="sortable-item-collapse-toggle"></span>

                <a href="#" class="sortable-item-delete" data-attr-confirm-text="<?php echo t('Are you sure'); ?>">
                    <i class="fa fa-times"></i>
                </a>

                <div class="sortable-item-handle">
                    <i class="fa fa-sort"></i>
                </div>
            </div>
        </script>
    </div>

<script type="text/javascript">
    Concrete.event.publish('btImgTextSet.repeater.edit.open', {id: '<?php echo $repeatable_container_id; ?>'});
    $.each($('#<?php echo $repeatable_container_id; ?> input[type="text"].title-me'), function () {
        $(this).trigger('keyup');
    });
</script>