<?php namespace Application\Block\ImgTextSet;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use Concrete\Core\Editor\LinkAbstractor;
use File;
use Page;
use Permissions;
use URL;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('repeater' => array());
    protected $btExportFileColumns = array('img');
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btImgTextSet', 'btImgTextSetRepeaterEntries');
    protected $btTable = 'btImgTextSet';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("タイトル+本文＋画像+リンク先をまとめて登録し、カスタムテンプレートで様々な形で出力します");
    }

    public function getBlockTypeName()
    {
        return t("汎用ブロック");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $repeater_items = $db->fetchAll('SELECT * FROM btImgTextSetRepeaterEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($repeater_items as $repeater_item_k => $repeater_item_v) {
            if (isset($repeater_item_v["ttl"]) && trim($repeater_item_v["ttl"]) != "") {
                $content[] = $repeater_item_v["ttl"];
            }
            if (isset($repeater_item_v["content"]) && trim($repeater_item_v["content"]) != "") {
                $content[] = $repeater_item_v["content"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $repeater = array();
        $repeater["ttlStyle_options"] = array(
            '' => "-- " . t("None") . " --",
            'large' => "タイトル大",
            'medium' => "タイトル中",
            'small' => "タイトル小"
        );
        $repeater_items = $db->fetchAll('SELECT * FROM btImgTextSetRepeaterEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($repeater_items as $repeater_item_k => &$repeater_item_v) {
            $repeater_item_v["content"] = isset($repeater_item_v["content"]) ? LinkAbstractor::translateFrom($repeater_item_v["content"]) : null;
            $repeater_item_v["link_Object"] = null;
			$repeater_item_v["link_Title"] = trim($repeater_item_v["link_Title"]);
			if (isset($repeater_item_v["link"]) && trim($repeater_item_v["link"]) != '') {
				switch ($repeater_item_v["link"]) {
					case 'page':
						if (!empty($repeater_item_v["link_Page"]) && ($repeater_item_v["link_Page_c"] = Page::getByID($repeater_item_v["link_Page"])) && !$repeater_item_v["link_Page_c"]->error && !$repeater_item_v["link_Page_c"]->isInTrash()) {
							$repeater_item_v["link_Object"] = $repeater_item_v["link_Page_c"];
							$repeater_item_v["link_URL"] = $repeater_item_v["link_Page_c"]->getCollectionLink();
							if ($repeater_item_v["link_Title"] == '') {
								$repeater_item_v["link_Title"] = $repeater_item_v["link_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$repeater_item_v["link_File_id"] = (int)$repeater_item_v["link_File"];
						if ($repeater_item_v["link_File_id"] > 0 && ($repeater_item_v["link_File_object"] = File::getByID($repeater_item_v["link_File_id"])) && is_object($repeater_item_v["link_File_object"])) {
							$fp = new Permissions($repeater_item_v["link_File_object"]);
							if ($fp->canViewFile()) {
								$repeater_item_v["link_Object"] = $repeater_item_v["link_File_object"];
								$repeater_item_v["link_URL"] = $repeater_item_v["link_File_object"]->getRelativePath();
								if ($repeater_item_v["link_Title"] == '') {
									$repeater_item_v["link_Title"] = $repeater_item_v["link_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($repeater_item_v["link_Title"] == '') {
							$repeater_item_v["link_Title"] = $repeater_item_v["link_URL"];
						}
						break;
				    case 'relative_url':
						if ($repeater_item_v["link_Title"] == '') {
							$repeater_item_v["link_Title"] = $repeater_item_v["link_Relative_URL"];
						}
						$repeater_item_v["link_URL"] = $repeater_item_v["link_Relative_URL"];
						break;
				    case 'image':
						if ($repeater_item_v["link_Image"] && ($repeater_item_v["link_Image_object"] = File::getByID($repeater_item_v["link_Image"])) && is_object($repeater_item_v["link_Image_object"])) {
							$repeater_item_v["link_URL"] = $repeater_item_v["link_Image_object"]->getURL();
							$repeater_item_v["link_Object"] = $repeater_item_v["link_Image_object"];
							if ($repeater_item_v["link_Title"] == '') {
								$repeater_item_v["link_Title"] = $repeater_item_v["link_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
            if (isset($repeater_item_v['img']) && trim($repeater_item_v['img']) != "" && ($f = File::getByID($repeater_item_v['img'])) && is_object($f)) {
                $repeater_item_v['img'] = $f;
            } else {
                $repeater_item_v['img'] = false;
            }
        }
        $this->set('repeater_items', $repeater_items);
        $this->set('repeater', $repeater);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btImgTextSetRepeaterEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $repeater_items = $db->fetchAll('SELECT * FROM btImgTextSetRepeaterEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($repeater_items as $repeater_item) {
            unset($repeater_item['id']);
            $repeater_item['bID'] = $newBID;
            $db->insert('btImgTextSetRepeaterEntries', $repeater_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $repeater = $this->get('repeater');
        $this->set('repeater_items', array());
        $this->set('repeater', $repeater);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $repeater = $this->get('repeater');
        $repeater_items = $db->fetchAll('SELECT * FROM btImgTextSetRepeaterEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        
        foreach ($repeater_items as &$repeater_item) {
            $repeater_item['content'] = isset($repeater_item['content']) ? LinkAbstractor::translateFromEditMode($repeater_item['content']) : null;
        }
        foreach ($repeater_items as &$repeater_item) {
            if (!File::getByID($repeater_item['img'])) {
                unset($repeater_item['img']);
            }
        }
        $this->set('repeater', $repeater);
        $this->set('repeater_items', $repeater_items);
    }

    protected function addEdit()
    {
        $repeater = array();
        $repeater['ttlStyle_options'] = array(
            '' => "-- " . t("None") . " --",
            'large' => "タイトル大",
            'medium' => "タイトル中",
            'small' => "タイトル小"
        );
        $this->set("link_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('repeater', $repeater);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/img_text_set/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/img_text_set/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/img_text_set/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btImgTextSetRepeaterEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $repeater_items = isset($args['repeater']) && is_array($args['repeater']) ? $args['repeater'] : array();
        $queries = array();
        if (!empty($repeater_items)) {
            $i = 0;
            foreach ($repeater_items as $repeater_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($repeater_item['ttl']) && trim($repeater_item['ttl']) != '') {
                    $data['ttl'] = trim($repeater_item['ttl']);
                } else {
                    $data['ttl'] = null;
                }
                if (isset($repeater_item['ttlStyle']) && trim($repeater_item['ttlStyle']) != '') {
                    $data['ttlStyle'] = trim($repeater_item['ttlStyle']);
                } else {
                    $data['ttlStyle'] = null;
                }
                $data['content'] = isset($repeater_item['content']) ? LinkAbstractor::translateTo($repeater_item['content']) : null;
                if (isset($repeater_item['link']) && trim($repeater_item['link']) != '') {
					$data['link_Title'] = $repeater_item['link_Title'];
					$data['link'] = $repeater_item['link'];
					switch ($repeater_item['link']) {
						case 'page':
							$data['link_Page'] = $repeater_item['link_Page'];
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'file':
							$data['link_File'] = $repeater_item['link_File'];
							$data['link_Page'] = '';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'url':
							$data['link_URL'] = $repeater_item['link_URL'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'relative_url':
							$data['link_Relative_URL'] = $repeater_item['link_Relative_URL'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'image':
							$data['link_Image'] = $repeater_item['link_Image'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							break;
                        default:
							$data['link'] = '';
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;	
					}
				}
				else {
					$data['link'] = '';
					$data['link_Title'] = '';
					$data['link_Page'] = '';
					$data['link_File'] = '0';
					$data['link_URL'] = '';
					$data['link_Relative_URL'] = '';
					$data['link_Image'] = '0';
				}
                if (isset($repeater_item['img']) && trim($repeater_item['img']) != '') {
                    $data['img'] = trim($repeater_item['img']);
                } else {
                    $data['img'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btImgTextSetRepeaterEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btImgTextSetRepeaterEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btImgTextSetRepeaterEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $repeaterEntriesMin = 0;
        $repeaterEntriesMax = 0;
        $repeaterEntriesErrors = 0;
        $repeater = array();
        if (isset($args['repeater']) && is_array($args['repeater']) && !empty($args['repeater'])) {
            if ($repeaterEntriesMin >= 1 && count($args['repeater']) < $repeaterEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("データグループ"), $repeaterEntriesMin, count($args['repeater'])));
                $repeaterEntriesErrors++;
            }
            if ($repeaterEntriesMax >= 1 && count($args['repeater']) > $repeaterEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("データグループ"), $repeaterEntriesMax, count($args['repeater'])));
                $repeaterEntriesErrors++;
            }
            if ($repeaterEntriesErrors == 0) {
                foreach ($args['repeater'] as $repeater_k => $repeater_v) {
                    if (is_array($repeater_v)) {
                        if (in_array("ttl", $this->btFieldsRequired['repeater']) && (!isset($repeater_v['ttl']) || trim($repeater_v['ttl']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("タイトル"), t("データグループ"), $repeater_k));
                        }
                        if ((in_array("ttlStyle", $this->btFieldsRequired['repeater']) && (!isset($repeater_v['ttlStyle']) || trim($repeater_v['ttlStyle']) == "")) || (isset($repeater_v['ttlStyle']) && trim($repeater_v['ttlStyle']) != "" && !in_array($repeater_v['ttlStyle'], array("large", "medium", "small")))) {
                            $e->add(t("The %s field has an invalid value (%s, row #%s).", t("タイトルスタイル"), t("データグループ"), $repeater_k));
                        }
                        if (in_array("content", $this->btFieldsRequired['repeater']) && (!isset($repeater_v['content']) || trim($repeater_v['content']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("本文"), t("データグループ"), $repeater_k));
                        }
                        if ((in_array("link", $this->btFieldsRequired['repeater']) && (!isset($repeater_v['link']) || trim($repeater_v['link']) == "")) || (isset($repeater_v['link']) && trim($repeater_v['link']) != "" && !array_key_exists($repeater_v['link'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク")));
						} elseif (array_key_exists($repeater_v['link'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($repeater_v['link']) {
								case 'page':
									if (!isset($repeater_v['link_Page']) || trim($repeater_v['link_Page']) == "" || $repeater_v['link_Page'] == "0" || (($page = Page::getByID($repeater_v['link_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク"), t("データグループ"), $repeater_k));
									}
									break;
				                case 'file':
									if (!isset($repeater_v['link_File']) || trim($repeater_v['link_File']) == "" || !is_object(File::getByID($repeater_v['link_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク"), t("データグループ"), $repeater_k));
									}
									break;
				                case 'url':
									if (!isset($repeater_v['link_URL']) || trim($repeater_v['link_URL']) == "" || !filter_var($repeater_v['link_URL'], FILTER_VALIDATE_URL)) {
										//$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク"), t("データグループ"), $repeater_k));
									}
									break;
				                case 'relative_url':
									if (!isset($repeater_v['link_Relative_URL']) || trim($repeater_v['link_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク"), t("データグループ"), $repeater_k));
									}
									break;
				                case 'image':
									if (!isset($repeater_v['link_Image']) || trim($repeater_v['link_Image']) == "" || !is_object(File::getByID($repeater_v['link_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク"), t("データグループ"), $repeater_k));
									}
									break;	
							}
						}
                        if (in_array("img", $this->btFieldsRequired['repeater']) && (!isset($repeater_v['img']) || trim($repeater_v['img']) == "" || !is_object(File::getByID($repeater_v['img'])))) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("画像"), t("データグループ"), $repeater_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('データグループ'), $repeater_k));
                    }
                }
            }
        } else {
            if ($repeaterEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("データグループ"), $repeaterEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-img_text_set', 'blocks/img_text_set/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-img_text_set');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}