<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($repeater_items)) { ?>
<div class="row">
	<?php foreach ($repeater_items as $repeater_item_key => $repeater_item) { ?>
	<?php if ($repeater_item["img"]) {
		$style = 'style="background-image:url('.$repeater_item["img"]->getURL().');background-posittion:center center;background-size: cover;"';
	}
	?>
		<div class="col-2" <?php echo $style;?>>
			<div class="home-eventBtn__left">
				<?php if (isset($repeater_item["ttl"]) && trim($repeater_item["ttl"]) != "") { ?>
				<h3><?php echo nl2br(h($repeater_item["ttl"])); ?></h3>
				<?php } ?>
				<?php if (isset($repeater_item["content"]) && trim($repeater_item["content"]) != "") { ?>
				<div class="home-eventBtn__text"><?php echo $repeater_item["content"]; ?></div>
				<?php } ?>

				<?php if (trim($repeater_item["link_URL"]) != "") { ?>
				<?php
				$repeater_itemlink_Attributes = array();
				$repeater_itemlink_Attributes['href'] = $repeater_item["link_URL"];
				$repeater_item["link_AttributesHtml"] = join(' ', array_map(function ($key) use ($repeater_itemlink_Attributes) {
					return $key . '="' . $repeater_itemlink_Attributes[$key] . '"';
				}, array_keys($repeater_itemlink_Attributes)));
				echo sprintf('<div class="btn_waku btn-color3"><a %s>%s</a></div>', $repeater_item["link_AttributesHtml"], $repeater_item["link_Title"]); ?><?php
				} ?>
			</div><!-- /home-eventBtn__left -->
		</div>
	<?php } ?>
</div>
<?php } ?>