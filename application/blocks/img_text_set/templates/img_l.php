<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($repeater_items)) { ?>

	<?php foreach ($repeater_items as $repeater_item_key => $repeater_item) { ?>
	<?php 
	switch($repeater_item['ttlStyle']) {
		case "large":
			$title = '<h2>'.$repeater_item["ttl"].'</h2>';
		break;
		case "medium":
			$title = '<h3>'.$repeater_item["ttl"].'</h3>';
		break;
		case "small":
			$title = '<h4>'.$repeater_item["ttl"].'</h4>';
		break;
		default:
			$title = '<h3>'.$repeater_item["ttl"].'</h3>';
		break;
	} 
	?>
	<div class="clearfix block-wrapper">
		<?php if ($repeater_item["img"]):?>
			<div class="block-img f_left">
				<img src="<?php echo $repeater_item["img"]->getURL();?>" alt="">
			</div>
		<?php endif;?>

		<div class="block-text f_right">
			<?php if($repeater_item["ttl"]):?>
				<?php echo $title ;?>
			<?php endif;?>
			<?php if (isset($repeater_item["content"]) && trim($repeater_item["content"]) != "") { ?>
				<?php echo $repeater_item["content"]; ?>
			<?php } ?>
		</div>
	</div>
	<?php } ?>

<?php } ?>



