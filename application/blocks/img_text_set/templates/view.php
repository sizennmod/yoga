<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($repeater_items)) { ?>
    <?php foreach ($repeater_items as $repeater_item_key => $repeater_item) { ?><?php if (isset($repeater_item["ttl"]) && trim($repeater_item["ttl"]) != "") { ?>
    <?php echo h($repeater_item["ttl"]); ?><?php } ?><?php if (isset($repeater_item["content"]) && trim($repeater_item["content"]) != "") { ?>
    <?php echo $repeater_item["content"]; ?><?php } ?><?php
if (trim($repeater_item["link_URL"]) != "") { ?>
    <?php
	$repeater_itemlink_Attributes = array();
	$repeater_itemlink_Attributes['href'] = $repeater_item["link_URL"];
	$repeater_item["link_AttributesHtml"] = join(' ', array_map(function ($key) use ($repeater_itemlink_Attributes) {
		return $key . '="' . $repeater_itemlink_Attributes[$key] . '"';
	}, array_keys($repeater_itemlink_Attributes)));
	echo sprintf('<a %s>%s</a>', $repeater_item["link_AttributesHtml"], $repeater_item["link_Title"]); ?><?php
} ?><?php if ($repeater_item["img"]) { ?>
    <img src="<?php echo $repeater_item["img"]->getURL(); ?>" alt="<?php echo $repeater_item["img"]->getTitle(); ?>"/><?php } ?><?php } ?><?php } ?>