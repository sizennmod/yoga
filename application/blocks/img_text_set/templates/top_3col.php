<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($repeater_items)) { ?>
<div class="home-eventBtn">
	<div class="row flex">
		<?php foreach ($repeater_items as $repeater_item_key => $repeater_item) { ?>

			<?php 
				if (is_object($repeater_item["img"])) {
					$style = 'style="background-image:url('.$repeater_item["img"]->getURL().');background-position:center center;background-size: cover;"';
				}else{
					$style = '';
				}
			?>
			<a <?php echo $style;?> href="<?php echo $repeater_item['link_URL'] ?>" class="col-3 home-eventBtn__bg3" <?php echo $row['openInNewWindow']  ?  'target="_blank"' : '' ?>>
			<?php echo nl2br(h($repeater_item["ttl"]));?>
			</a>
		<?php } ?>
	</div>
</div>
<?php } ?>