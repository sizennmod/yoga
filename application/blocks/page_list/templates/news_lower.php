<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */

$pages_chunk = array_chunk($pages, 4);
?>
<?php if ($c->isEditMode() && $controller->isBlockEmpty()) {?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>
	<?php foreach($pages_chunk as $pages):?>
		<div class="container--news_lower">
			<ul class="row">
			<?php
			foreach ($pages as $page):

			// Prepare data for each page being listed...
			
			$title = $th->entities($page->getCollectionName());
			$url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
			$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
			$target = empty($target) ? '_self' : $target;
			
			$thumbnail = false;
			if ($displayThumbnail) {
				$thumbnail = $page->getAttribute('thumbnail');
			}
			$date = $page->getCollectionDatePublic();
			$date = date('Y.m.d',strtotime($date));
			//Other useful page data...

			//$last_edited_by = $page->getVersionObject()->getVersionAuthorUserName();

			//$original_author = Page::getByID($page->getCollectionID(), 1)->getVersionObject()->getVersionAuthorUserName();

			/* CUSTOM ATTRIBUTE EXAMPLES:
			 * $example_value = $page->getAttribute('example_attribute_handle');
			 *
			 * HOW TO USE IMAGE ATTRIBUTES:
			 * 1) Uncomment the "$ih = Loader::helper('image');" line up top.
			 * 2) Put in some code here like the following 2 lines:
			 *      $img = $page->getAttribute('example_image_attribute_handle');
			 *      $thumb = $ih->getThumbnail($img, 64, 9999, false);
			 *    (Replace "64" with max width, "9999" with max height. The "9999" effectively means "no maximum size" for that particular dimension.)
			 *    (Change the last argument from false to true if you want thumbnails cropped.)
			 * 3) Output the image tag below like this:
			 *      <img src="<?php echo $thumb->src ?>" width="<?php echo $thumb->width ?>" height="<?php echo $thumb->height ?>" alt="" />
			 *
			 * ~OR~ IF YOU DO NOT WANT IMAGES TO BE RESIZED:
			 * 1) Put in some code here like the following 2 lines:
			 *      $img_src = $img->getRelativePath();
			 *      $img_width = $img->getAttribute('width');
			 *      $img_height = $img->getAttribute('height');
			 * 2) Output the image tag below like this:
			 *      <img src="<?php echo $img_src ?>" width="<?php echo $img_width ?>" height="<?php echo $img_height ?>" alt="" />
			 */

			$blocks = $page->getBlocks('メインコンテンツ');
			// filter by block type handle
			$_blocks = array();
			foreach($blocks as $block){
				if($block->btHandle == 'content'){
					$_blocks[] = $block;
					break;
				}
			}

			$blocks = $_blocks;
			// get instance of the block
			if (is_object($blocks[0])) {
				$bc = mb_substr(strip_tags ($blocks[0]->getInstance()->getContent()), 0 , 36);
			}
		
			if (is_object($thumbnail)){
				$img_src = $thumbnail->getRecentVersion()->getThumbnailURL('thmb_234_130_2x');
			}else{
				$img_src = $this->getThemePath().'/assets/img/news_sample_img.jpg';
			} 

			?>
				<li>
					<div class="news__news-thumb">
						<a href="<?php echo $url?>" style="background-image:url(<?php echo $img_src;?>) ">
							<img src="<?php echo $img_src;?>" alt="<?php echo $title;?>"/>
						</a>
					</div>
					<div class="news__news-block">
						<h4><?php echo $title;?></h4>
						<?php if($bc) echo '<p>'.$bc.'</p>';?>
						<p class="news-date"><?php echo $date;?></p>
					</div>
				</li>
			<?php endforeach;?>
			</ul>
		</div>
	<?php endforeach;?>
	<?php if ($showPagination): ?>
		<?php
		$pagination = $list->getPagination();
		if ($pagination->getTotalPages() > 1) {
			$options = array(
				'proximity'           => 2,
				'prev_message'        => '<<',
				'next_message'        => '>>',
				'dots_message'        => '...',
				'active_suffix'       => '',
				'css_container_class' => 'mp_container-class',
				'css_prev_class'      => 'mp_prev-class',
				'css_next_class'      => 'mp_next-class',
				'css_disabled_class'  => 'mp_disabled-class',
				'css_dots_class'      => 'mp_dots-class',
				'css_active_class'    => 'mp_active-class'
			);
			echo $pagination->renderDefaultView($options);
		}
		?>
	<?php endif; ?>
<?php } ?>
