<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>
<?php if ($c->isEditMode() && $controller->isBlockEmpty()) {?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>
	<div class="swiper-container--news" id="swiper-<?php echo $bID;?>">
		<ul class="swiper-wrapper row">
		<?php
		foreach ($pages as $page):

			// Prepare data for each page being listed...
			
			$title = $th->entities($page->getCollectionName());
			$url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
			$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
			$target = empty($target) ? '_self' : $target;
			
			$thumbnail = false;
			if ($displayThumbnail) {
				$thumbnail = $page->getAttribute('thumbnail');
			}
			$date = $page->getCollectionDatePublic();
			$date = date('Y.m.d',strtotime($date));
		//Other useful page data...

		//$last_edited_by = $page->getVersionObject()->getVersionAuthorUserName();

		//$original_author = Page::getByID($page->getCollectionID(), 1)->getVersionObject()->getVersionAuthorUserName();

		/* CUSTOM ATTRIBUTE EXAMPLES:
		 * $example_value = $page->getAttribute('example_attribute_handle');
		 *
		 * HOW TO USE IMAGE ATTRIBUTES:
		 * 1) Uncomment the "$ih = Loader::helper('image');" line up top.
		 * 2) Put in some code here like the following 2 lines:
		 *      $img = $page->getAttribute('example_image_attribute_handle');
		 *      $thumb = $ih->getThumbnail($img, 64, 9999, false);
		 *    (Replace "64" with max width, "9999" with max height. The "9999" effectively means "no maximum size" for that particular dimension.)
		 *    (Change the last argument from false to true if you want thumbnails cropped.)
		 * 3) Output the image tag below like this:
		 *		<img src="<?php echo $thumb->src ?>" width="<?php echo $thumb->width ?>" height="<?php echo $thumb->height ?>" alt="" />
		 *
		 * ~OR~ IF YOU DO NOT WANT IMAGES TO BE RESIZED:
		 * 1) Put in some code here like the following 2 lines:
		 * 	    $img_src = $img->getRelativePath();
		 *      $img_width = $img->getAttribute('width');
		 *      $img_height = $img->getAttribute('height');
		 * 2) Output the image tag below like this:
		 * 	    <img src="<?php echo $img_src ?>" width="<?php echo $img_width ?>" height="<?php echo $img_height ?>" alt="" />
		 */

		$blocks = $page->getBlocks('メインコンテンツ');
		// filter by block type handle
		$_blocks = array();
		foreach($blocks as $block){
			if($block->btHandle == 'content'){
				$_blocks[] = $block;
				break;
			}
		}

		$blocks = $_blocks;
		// get instance of the block
		if (is_object($blocks[0])) {
			$bc = mb_substr(strip_tags ($blocks[0]->getInstance()->getContent()), 0 , 36);
		}
		$img_src = '';
		if (is_object($thumbnail)){
			$img_src = $thumbnail->getRecentVersion()->getThumbnailURL('thmb_234_130_2x');
		} ?>
		<li class="swiper-slide">
			<div class="home-news__news-thumb">
				<a href="<?php echo $url?>">
					<?php if($img_src):?>
					<img src="<?php echo $img_src;?>" alt="<?php echo $title;?>"/>
					<?php else:?>
					<img src="<?php echo $this->getThemePath()?>/assets/img/news_sample_img.jpg" alt="<?php echo $title;?>"/>
					<?php endif;?>
				</a>
			</div>
			<div class="home-news__news-block">
				<h4><?php echo $title;?></h4>
				<?php if($bc) echo '<p>'.$bc.'</p>';?>
				<p class="news-date"><?php echo $date;?></p>
			</div>
		</li>
	<?php endforeach;?>
	</ul>
	<div class="swiper__ctrl--next swiper-button-next-<?php echo $bID;?>"></div>
	<div class="swiper__ctrl--prev swiper-button-prev-<?php echo $bID;?>"></div>
	</div>

	<script>
	$(document).ready(function($) {
		var swiper = new Swiper('#swiper-<?php echo $bID;?>', {
			speed:800,
			slidesPerView: 4,
			paginationClickable: true,
			spaceBetween: 10,
			loop:true,
			autoplay:3000,
			nextButton: '.swiper-button-next-<?php echo $bID;?>',
			prevButton: '.swiper-button-prev-<?php echo $bID;?>',
			breakpoints: {
				// when window width is <= 640px
				768: {
					slidesPerView: 2
				}
			}
		});
	});
	</script>
<?php } ?>
