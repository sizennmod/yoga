<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<div class="ext_title">
	<h2>
	<?php if (isset($mainttl) && trim($mainttl) != "") { ?>
		<?php echo h($mainttl); ?>
	<?php } ?>
	<?php if (isset($subttl) && trim($subttl) != "") { ?>
		<div class="subttl"><?php echo h($subttl); ?></div>
	<?php } ?>
	</h2>
</div>