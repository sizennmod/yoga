<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php echo $form->label('mainttl', t("メインタイトル")); ?>
    <?php echo isset($btFieldsRequired) && in_array('mainttl', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('mainttl'), $mainttl, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php echo $form->label('subttl', t("サブタイトル")); ?>
    <?php echo isset($btFieldsRequired) && in_array('subttl', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('subttl'), $subttl, array (
  'maxlength' => 255,
)); ?>
</div>