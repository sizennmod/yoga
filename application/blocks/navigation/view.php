<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (!empty($navloop_items)) { ?>
    <?php foreach ($navloop_items as $navloop_item_key => $navloop_item) { ?><?php if (isset($navloop_item["labelMain"]) && trim($navloop_item["labelMain"]) != "") { ?>
    <?php echo h($navloop_item["labelMain"]); ?><?php } ?><?php if (isset($navloop_item["labelSub"]) && trim($navloop_item["labelSub"]) != "") { ?>
    <?php echo h($navloop_item["labelSub"]); ?><?php } ?><?php
if (trim($navloop_item["link_URL"]) != "") { ?>
    <?php
	$navloop_itemlink_Attributes = array();
	$navloop_itemlink_Attributes['href'] = $navloop_item["link_URL"];
	$navloop_item["link_AttributesHtml"] = join(' ', array_map(function ($key) use ($navloop_itemlink_Attributes) {
		return $key . '="' . $navloop_itemlink_Attributes[$key] . '"';
	}, array_keys($navloop_itemlink_Attributes)));
	echo sprintf('<a %s>%s</a>', $navloop_item["link_AttributesHtml"], $navloop_item["link_Title"]); ?><?php
} ?><?php } ?><?php } ?>