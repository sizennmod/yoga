<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php 
$nh = Core::make('helper/navigation');
if (!empty($navloop_items)) { ?>
	<?php foreach ($navloop_items as $navloop_item_key => $navloop_item) { ?>
		<?php
		if (isset($navloop_item["labelMain"]) && trim($navloop_item["labelMain"]) != "") { 
			$labelMain = h($navloop_item["labelMain"]); 
		}
		if (isset($navloop_item["labelSub"]) && trim($navloop_item["labelSub"]) != "") { 
			$labelSub = h($navloop_item["labelSub"]); 
		} 
		
		$navloop_itemlink_Attributes = array();
		$navloop_itemlink_Attributes['href'] = $navloop_item["link_URL"];
		$navloop_item["link_AttributesHtml"] = join(' ', array_map(function ($key) use ($navloop_itemlink_Attributes) {
			return $key . '="' . $navloop_itemlink_Attributes[$key] . '"';
		}, array_keys($navloop_itemlink_Attributes)));
		
		if($navloop_item["showChild"] > 0):
			$list = new \Concrete\Core\Page\PageList();
			$list->filter('p.cParentID', $navloop_item["link_Page"]);
			$pages = $list->getResults();
		?>
			<li>
				<a <?php echo $navloop_item["link_AttributesHtml"];?>><span><?php echo $labelMain;?></span>　<?php echo $labelSub;?></a>
				<?php if($pages):?>
					<ul class="child">
						<?php foreach($pages as $page):?>
						<li>
							<a href="<?php echo $nh->getLinkToCollection($page);?>">
							<?php echo h($page->getCollectionName());?>
							</a>
						</li>
						<?php endforeach;?>
					</ul>
				<?php endif;?>
			</li>
		<?php else:?>
			<?php echo sprintf('<li><a %s><span>%s</span>　%s</a></li>', $navloop_item["link_AttributesHtml"], $labelMain,$labelSub);  ?>
		<?php endif;?>
	<?php } ?>
<?php } ?>

