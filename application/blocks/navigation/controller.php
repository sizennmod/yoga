<?php namespace Application\Block\Navigation;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Database;
use File;
use Page;
use Permissions;
use URL;
use AssetList;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('navloop' => array());
    protected $btExportFileColumns = array();
    protected $btExportPageColumns = array();
    protected $btExportTables = array('btNavigation', 'btNavigationNavloopEntries');
    protected $btTable = 'btNavigation';
    protected $btInterfaceWidth = 800;
    protected $btInterfaceHeight = 600;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $btDefaultSet = 'navigation';
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("ナビゲーション");
    }

    public function getSearchableContent()
    {
        $content = array();
        $db = Database::connection();
        $navloop_items = $db->fetchAll('SELECT * FROM btNavigationNavloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($navloop_items as $navloop_item_k => $navloop_item_v) {
            if (isset($navloop_item_v["labelMain"]) && trim($navloop_item_v["labelMain"]) != "") {
                $content[] = $navloop_item_v["labelMain"];
            }
            if (isset($navloop_item_v["labelSub"]) && trim($navloop_item_v["labelSub"]) != "") {
                $content[] = $navloop_item_v["labelSub"];
            }
        }
        return implode(" ", $content);
    }

    public function view()
    {
        $db = Database::connection();
        $navloop = array();
        $navloop_items = $db->fetchAll('SELECT * FROM btNavigationNavloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($navloop_items as $navloop_item_k => &$navloop_item_v) {
            $navloop_item_v["link_Object"] = null;
			$navloop_item_v["link_Title"] = trim($navloop_item_v["link_Title"]);
			if (isset($navloop_item_v["link"]) && trim($navloop_item_v["link"]) != '') {
				switch ($navloop_item_v["link"]) {
					case 'page':
						if (!empty($navloop_item_v["link_Page"]) && ($navloop_item_v["link_Page_c"] = Page::getByID($navloop_item_v["link_Page"])) && !$navloop_item_v["link_Page_c"]->error && !$navloop_item_v["link_Page_c"]->isInTrash()) {
							$navloop_item_v["link_Object"] = $navloop_item_v["link_Page_c"];
							$navloop_item_v["link_URL"] = $navloop_item_v["link_Page_c"]->getCollectionLink();
							if ($navloop_item_v["link_Title"] == '') {
								$navloop_item_v["link_Title"] = $navloop_item_v["link_Page_c"]->getCollectionName();
							}
						}
						break;
				    case 'file':
						$navloop_item_v["link_File_id"] = (int)$navloop_item_v["link_File"];
						if ($navloop_item_v["link_File_id"] > 0 && ($navloop_item_v["link_File_object"] = File::getByID($navloop_item_v["link_File_id"])) && is_object($navloop_item_v["link_File_object"])) {
							$fp = new Permissions($navloop_item_v["link_File_object"]);
							if ($fp->canViewFile()) {
								$navloop_item_v["link_Object"] = $navloop_item_v["link_File_object"];
								$navloop_item_v["link_URL"] = $navloop_item_v["link_File_object"]->getRelativePath();
								if ($navloop_item_v["link_Title"] == '') {
									$navloop_item_v["link_Title"] = $navloop_item_v["link_File_object"]->getTitle();
								}
							}
						}
						break;
				    case 'url':
						if ($navloop_item_v["link_Title"] == '') {
							$navloop_item_v["link_Title"] = $navloop_item_v["link_URL"];
						}
						break;
				    case 'relative_url':
						if ($navloop_item_v["link_Title"] == '') {
							$navloop_item_v["link_Title"] = $navloop_item_v["link_Relative_URL"];
						}
						$navloop_item_v["link_URL"] = $navloop_item_v["link_Relative_URL"];
						break;
				    case 'image':
						if ($navloop_item_v["link_Image"] && ($navloop_item_v["link_Image_object"] = File::getByID($navloop_item_v["link_Image"])) && is_object($navloop_item_v["link_Image_object"])) {
							$navloop_item_v["link_URL"] = $navloop_item_v["link_Image_object"]->getURL();
							$navloop_item_v["link_Object"] = $navloop_item_v["link_Image_object"];
							if ($navloop_item_v["link_Title"] == '') {
								$navloop_item_v["link_Title"] = $navloop_item_v["link_Image_object"]->getTitle();
							}
						}
						break;
				}
			}
        }
        $this->set('navloop_items', $navloop_items);
        $this->set('navloop', $navloop);
    }

    public function delete()
    {
        $db = Database::connection();
        $db->delete('btNavigationNavloopEntries', array('bID' => $this->bID));
        parent::delete();
    }

    public function duplicate($newBID)
    {
        $db = Database::connection();
        $navloop_items = $db->fetchAll('SELECT * FROM btNavigationNavloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        foreach ($navloop_items as $navloop_item) {
            unset($navloop_item['id']);
            $navloop_item['bID'] = $newBID;
            $db->insert('btNavigationNavloopEntries', $navloop_item);
        }
        parent::duplicate($newBID);
    }

    public function add()
    {
        $this->addEdit();
        $navloop = $this->get('navloop');
        $this->set('navloop_items', array());
        
        $this->set('navloop', $navloop);
    }

    public function edit()
    {
        $db = Database::connection();
        $this->addEdit();
        $navloop = $this->get('navloop');
        $navloop_items = $db->fetchAll('SELECT * FROM btNavigationNavloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $this->set('navloop', $navloop);
        $this->set('navloop_items', $navloop_items);
    }

    protected function addEdit()
    {
        $navloop = array();
        $this->set("link_Options", $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
), true));
        $this->set('navloop', $navloop);
        $this->set('identifier', new \Concrete\Core\Utility\Service\Identifier());
        $al = AssetList::getInstance();
        $al->register('css', 'repeatable-ft.form', 'blocks/navigation/css_form/repeatable-ft.form.css', array(), $this->pkg);
        $al->register('javascript', 'handlebars', 'blocks/navigation/js_form/handlebars-v4.0.4.js', array(), $this->pkg);
        $al->register('javascript', 'handlebars-helpers', 'blocks/navigation/js_form/handlebars-helpers.js', array(), $this->pkg);
        $this->requireAsset('core/sitemap');
        $this->requireAsset('css', 'repeatable-ft.form');
        $this->requireAsset('javascript', 'handlebars');
        $this->requireAsset('javascript', 'handlebars-helpers');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $db = Database::connection();
        $rows = $db->fetchAll('SELECT * FROM btNavigationNavloopEntries WHERE bID = ? ORDER BY sortOrder', array($this->bID));
        $navloop_items = isset($args['navloop']) && is_array($args['navloop']) ? $args['navloop'] : array();
        $queries = array();
        if (!empty($navloop_items)) {
            $i = 0;
            foreach ($navloop_items as $navloop_item) {
                $data = array(
                    'sortOrder' => $i + 1,
                );
                if (isset($navloop_item['labelMain']) && trim($navloop_item['labelMain']) != '') {
                    $data['labelMain'] = trim($navloop_item['labelMain']);
                } else {
                    $data['labelMain'] = null;
                }
                if (isset($navloop_item['labelSub']) && trim($navloop_item['labelSub']) != '') {
                    $data['labelSub'] = trim($navloop_item['labelSub']);
                } else {
                    $data['labelSub'] = null;
                }
                if (isset($navloop_item['link']) && trim($navloop_item['link']) != '') {
					$data['link_Title'] = $navloop_item['link_Title'];
					$data['link'] = $navloop_item['link'];
					switch ($navloop_item['link']) {
						case 'page':
							$data['link_Page'] = $navloop_item['link_Page'];
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'file':
							$data['link_File'] = $navloop_item['link_File'];
							$data['link_Page'] = '';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'url':
							$data['link_URL'] = $navloop_item['link_URL'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'relative_url':
							$data['link_Relative_URL'] = $navloop_item['link_Relative_URL'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Image'] = '0';
							break;
                        case 'image':
							$data['link_Image'] = $navloop_item['link_Image'];
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							break;
                        default:
							$data['link'] = '';
							$data['link_Page'] = '';
							$data['link_File'] = '0';
							$data['link_URL'] = '';
							$data['link_Relative_URL'] = '';
							$data['link_Image'] = '0';
							break;	
					}
				}
				else {
					$data['link'] = '';
					$data['link_Title'] = '';
					$data['link_Page'] = '';
					$data['link_File'] = '0';
					$data['link_URL'] = '';
					$data['link_Relative_URL'] = '';
					$data['link_Image'] = '0';
				}
                if (isset($navloop_item['showChild']) && trim($navloop_item['showChild']) != '' && in_array($navloop_item['showChild'], array(0, 1))) {
                    $data['showChild'] = $navloop_item['showChild'];
                } else {
                    $data['showChild'] = null;
                }
                if (isset($rows[$i])) {
                    $queries['update'][$rows[$i]['id']] = $data;
                    unset($rows[$i]);
                } else {
                    $data['bID'] = $this->bID;
                    $queries['insert'][] = $data;
                }
                $i++;
            }
        }
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $queries['delete'][] = $row['id'];
            }
        }
        if (!empty($queries)) {
            foreach ($queries as $type => $values) {
                if (!empty($values)) {
                    switch ($type) {
                        case 'update':
                            foreach ($values as $id => $data) {
                                $db->update('btNavigationNavloopEntries', $data, array('id' => $id));
                            }
                            break;
                        case 'insert':
                            foreach ($values as $data) {
                                $db->insert('btNavigationNavloopEntries', $data);
                            }
                            break;
                        case 'delete':
                            foreach ($values as $value) {
                                $db->delete('btNavigationNavloopEntries', array('id' => $value));
                            }
                            break;
                    }
                }
            }
        }
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        $navloopEntriesMin = 0;
        $navloopEntriesMax = 0;
        $navloopEntriesErrors = 0;
        $navloop = array();
        if (isset($args['navloop']) && is_array($args['navloop']) && !empty($args['navloop'])) {
            if ($navloopEntriesMin >= 1 && count($args['navloop']) < $navloopEntriesMin) {
                $e->add(t("The %s field requires at least %s entries, %s entered.", t("ナビゲーション"), $navloopEntriesMin, count($args['navloop'])));
                $navloopEntriesErrors++;
            }
            if ($navloopEntriesMax >= 1 && count($args['navloop']) > $navloopEntriesMax) {
                $e->add(t("The %s field is set to a maximum of %s entries, %s entered.", t("ナビゲーション"), $navloopEntriesMax, count($args['navloop'])));
                $navloopEntriesErrors++;
            }
            if ($navloopEntriesErrors == 0) {
                foreach ($args['navloop'] as $navloop_k => $navloop_v) {
                    if (is_array($navloop_v)) {
                        if (in_array("labelMain", $this->btFieldsRequired['navloop']) && (!isset($navloop_v['labelMain']) || trim($navloop_v['labelMain']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("メインラベル"), t("ナビゲーション"), $navloop_k));
                        }
                        if (in_array("labelSub", $this->btFieldsRequired['navloop']) && (!isset($navloop_v['labelSub']) || trim($navloop_v['labelSub']) == "")) {
                            $e->add(t("The %s field is required (%s, row #%s).", t("サブラベル"), t("ナビゲーション"), $navloop_k));
                        }
                        if ((in_array("link", $this->btFieldsRequired['navloop']) && (!isset($navloop_v['link']) || trim($navloop_v['link']) == "")) || (isset($navloop_v['link']) && trim($navloop_v['link']) != "" && !array_key_exists($navloop_v['link'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
))))) {
							$e->add(t("The %s field has an invalid value.", t("リンク先")));
						} elseif (array_key_exists($navloop_v['link'], $this->getSmartLinkTypeOptions(array (
  0 => 'page',
  1 => 'file',
  2 => 'image',
  3 => 'url',
  4 => 'relative_url',
)))) {
							switch ($navloop_v['link']) {
								case 'page':
									if (!isset($navloop_v['link_Page']) || trim($navloop_v['link_Page']) == "" || $navloop_v['link_Page'] == "0" || (($page = Page::getByID($navloop_v['link_Page'])) && $page->error !== false)) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Page"), t("リンク先"), t("ナビゲーション"), $navloop_k));
									}
									break;
				                case 'file':
									if (!isset($navloop_v['link_File']) || trim($navloop_v['link_File']) == "" || !is_object(File::getByID($navloop_v['link_File']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("File"), t("リンク先"), t("ナビゲーション"), $navloop_k));
									}
									break;
				                case 'url':
									if (!isset($navloop_v['link_URL']) || trim($navloop_v['link_URL']) == "" || !filter_var($navloop_v['link_URL'], FILTER_VALIDATE_URL)) {
										$e->add(t("The %s field for '%s' does not have a valid URL (%s, row #%s).", t("URL"), t("リンク先"), t("ナビゲーション"), $navloop_k));
									}
									break;
				                case 'relative_url':
									if (!isset($navloop_v['link_Relative_URL']) || trim($navloop_v['link_Relative_URL']) == "") {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Relative URL"), t("リンク先"), t("ナビゲーション"), $navloop_k));
									}
									break;
				                case 'image':
									if (!isset($navloop_v['link_Image']) || trim($navloop_v['link_Image']) == "" || !is_object(File::getByID($navloop_v['link_Image']))) {
										$e->add(t("The %s field for '%s' is required (%s, row #%s).", t("Image"), t("リンク先"), t("ナビゲーション"), $navloop_k));
									}
									break;	
							}
						}
                        if (in_array("showChild", $this->btFieldsRequired['navloop']) && (!isset($navloop_v['showChild']) || trim($navloop_v['showChild']) == "" || !in_array($navloop_v['showChild'], array(0, 1)))) {
                            $e->add(t("The %s field is required.", t("子ページを表示"), t("ナビゲーション"), $navloop_k));
                        }
                    } else {
                        $e->add(t("The values for the %s field, row #%s, are incomplete.", t('ナビゲーション'), $navloop_k));
                    }
                }
            }
        } else {
            if ($navloopEntriesMin >= 1) {
                $e->add(t("The %s field requires at least %s entries, none entered.", t("ナビゲーション"), $navloopEntriesMin));
            }
        }
        return $e;
    }

    public function composer()
    {
        $al = AssetList::getInstance();
        $al->register('javascript', 'auto-js-navigation', 'blocks/navigation/auto.js', array(), $this->pkg);
        $this->requireAsset('javascript', 'auto-js-navigation');
        $this->edit();
    }

        protected function getSmartLinkTypeOptions($include = array(), $checkNone = false)
	{
		$options = array(
			''             => sprintf("-- %s--", t("None")),
			'page'         => t("Page"),
			'url'          => t("External URL"),
			'relative_url' => t("Relative URL"),
			'file'         => t("File"),
			'image'        => t("Image")
		);
		if ($checkNone) {
            $include = array_merge(array(''), $include);
        }
		$return = array();
		foreach($include as $v){
		    if(isset($options[$v])){
		        $return[$v] = $options[$v];
		    }
		}
		return $return;
	}
}