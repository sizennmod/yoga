<?php defined('C5_EXECUTE') or die("Access Denied.");
if (is_object($f) && $f->getFileID()) {
    if ($maxWidth > 0 || $maxHeight > 0) {
        $crop = false;

        $im = Core::make('helper/image');
        $thumb = $im->getThumbnail($f, $maxWidth, $maxHeight, $crop);

        $tag = new \HtmlObject\Image();
        $tag->src($thumb->src)->width($thumb->width)->height($thumb->height);
    } else {
        $image = Core::make('html/image', [$f]);
        $tag = $image->getTag();
    }

    $tag->addClass('ccm-image-block img-responsive bID-'.$bID);
    if ($altText) {
        $tag->alt(h($altText));
    } else {
        $tag->alt('');
    }

    if ($title) {
        $tag->title(h($title));
    }
?>
<div class="fullwidth_img">
    <div class="full_width" style="background-image: url(<?php echo $f->getURL();?>);background-position: center center;">
    <img src="<?php echo $f->getURL();?>" alt="" class="sp-img">
    </div>
</div>
<?php
} elseif ($c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Image Block.') ?></div>
    <?php
}
if (is_object($foS)): ?>
<?php
endif;
