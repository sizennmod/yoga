<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php echo $form->label('anchorID', t("ID") . ' <i class="fa fa-question-circle launch-tooltip" data-original-title="' . t("アンカーとして利用するIDを必ず半角英数字で入力して下さい。") . '"></i>'); ?>
    <?php echo isset($btFieldsRequired) && in_array('anchorID', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('anchorID'), $anchorID, array (
  'maxlength' => 15,
  'placeholder' => 'content1',
)); ?>
</div>