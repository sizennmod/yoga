<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (isset($anchorID) && trim($anchorID) != "") { ?>
	<?php 
		$c = Page::getCurrentPage();
		if ($c->isEditMode()):
	?>
		<div class="anchor_txt">ここに<span>「<?php echo h($anchorID); ?>」</span>アンカーが設定されています。</div>
		<div class="anchor_cause">アンカー名が同一ページ内で重複すると動作しません。アンカー名は必ず別のIDを指定してください。
		</div>
	<?php else:?>
		<a id="<?php echo h($anchorID); ?>" name="<?php echo h($anchorID); ?>" class="anch"></a>
	<?php endif;?>
<?php } ?>