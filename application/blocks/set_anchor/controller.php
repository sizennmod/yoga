<?php namespace Application\Block\SetAnchor;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array();
    protected $btExportFileColumns = array();
    protected $btExportPageColumns = array();
    protected $btTable = 'btSetAnchor';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("アンカーリンクを設置します");
    }

    public function getBlockTypeName()
    {
        return t("アンカーリンク");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->anchorID;
        return implode(" ", $content);
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
    }

    protected function addEdit()
    {
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("anchorID", $this->btFieldsRequired) && (trim($args["anchorID"]) == "")) {
            $e->add(t("The %s field is required.", t("ID")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}