<?php
defined('C5_EXECUTE') or die("Access Denied.");
$u = new User();
$c = Page::getCurrentPage();

if($u->isLoggedIn()){
    echo 'このページは非ログイン時'.$rows[0]['linkURL'].'にリダイレクトします。';
}else{
    header( "HTTP/1.1 301 Moved Permanently" ); 
    header( "Location: ".$rows[0]['linkURL'] ); 
    exit;
}

?>
