<?php
defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
?>

<?php if (count($rows) > 0) {?>
<ul class="row">
	<?php foreach ($rows as $row) { ?>
		<?php
		// create title
		$title = null;
		if ($row['title'] != null) {
			$title = $row['title'];
		} elseif ($row['collectionName'] != null) {
			$title = $row['collectionName'];
		} else {
			$title = '';
		}
		
		$tag = "";
		if ($displayImage != 0) {
			if (is_object($row['image'])) {
				$src = $row['image']->getRecentVersion()->getThumbnailURL('thmb_150_150_2x');
				//retina：file_manager_listing_2x
			}
		}
		?>
		<li class="col-5">
			<a href="<?php echo $row['linkURL'] ?>" <?php echo $row['openInNewWindow']  ?  'target="_blank"' : '' ?> title="<?php echo h($title);?>">
				<?php if($src) :?>
					<img src="<?php echo $src;?>" alt="<?php echo h($title);?>">
					<?php if($title):?>
					<span style="display: block;font-size: 12px;text-align: center;"><?php echo h($title);?></span>
					<?php endif;?>
				<?php endif;?>
			</a>
		</li>
	<?php } ?>
</ul>
<?php } else { ?>
	<div class="ccm-manual-nav-placeholder">
		<p><?php echo t('No nav Entered.'); ?></p>
	</div>
<?php } ?>
