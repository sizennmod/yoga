#!/bin/sh

# c5_sync_db
# リモートのDB同期とファイル取得
#【注意】ssh接続できるのが前提です。
# mysqlコマンドが利用できること
# Pipe Viewerをインストールしておく

#取得元DBユーザー
FROMUSER='yogaosaka_admin'
#取得元DBパスワード
FROMPASWORD='3lmCncbJLL1pW98p'
#取得元DB名
FROMDBNAME='yogaosaka_deploy'
#ローカルバックアップ先
BACKUPTO='/Users/size-nn-mod/Dropbox-suitoshasano/Dropbox/yoga/yoga.sql'
#ローカルDBホスト ※mampなのでソケット指定
MYSQLIMPORTTO='-S /Applications/MAMP/tmp/mysql/mysql.sock'
#ローカルDBユーザー
IMPORTUSER='root'
#ローカルDBパスワード
IMPORTPASS='root'
#ローカルDB名
IMPORTDBNAME='yoga'

ssh yoga "mysqldump -hmysql2206.xserver.jp -u${FROMUSER} -p${FROMPASWORD} --single-transaction ${FROMDBNAME}" > ${BACKUPTO}
sed -i -e "s/\"\/packages/\"\/yoga\/packages/g" /Users/size-nn-mod/Dropbox-suitoshasano/Dropbox/yoga/yoga.sql
pv ${BACKUPTO} | mysql ${MYSQLIMPORTTO} -u${IMPORTUSER} -p${IMPORTPASS} ${IMPORTDBNAME}


#ここからアップロードディレクトリ（application/files）の取得
#転送先 環境に合わせて変更
TO="/Applications/MAMP/htdocs/yoga/application/files"
#転送元 自身の環境にあわせて変更
FROM="yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html/application/files/"

rsync -e "ssh -p10022 -i /Users/size-nn-mod/.ssh/yogaosaka.key" -av --delete ${FROM} ${TO}

#configオーバーライド情報の取得
#リモートでcongif設定変更をする場合は下記のコメントアウトを解除
#転送先 環境に合わせて変更
TO="/Applications/MAMP/htdocs/yoga/application/config/generated_overrides"
#転送元 自身の環境にあわせて変更
FROM="yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html/application/config/generated_overrides/"

rsync -e "ssh -p10022 -i /Users/size-nn-mod/.ssh/yogaosaka.key" -av --delete ${FROM} ${TO}

#言語ファイル
#転送先 環境に合わせて変更
TO="/Applications/MAMP/htdocs/yoga/application/languages"
#転送元 自身の環境にあわせて変更
FROM="yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html/application/languages/"

rsync -e "ssh -p10022 -i /Users/size-nn-mod/.ssh/yogaosaka.key" -av --delete ${FROM} ${TO}

sed -i -e "s/https:\/\/yogadaykansai.com/http:\/\/192.168.11.43:8888\/yoga/g" /Applications/MAMP/htdocs/yoga/application/config/generated_overrides/site.php

/Applications/MAMP/htdocs/yoga/concrete/bin/concrete5 c5:clear-cache