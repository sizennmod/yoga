#!/bin/sh
#【注意】ssh接続できるのが前提です。
#sshpass用のパスワード

MYENV='/Applications/MAMP/htdocs/yoga'
REMOTEDEFAULT='yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html'
EXCLUDE=("**/.DS_Store" ".git" ".gitignore" "**/sh")
for i in "${!EXCLUDE[@]}"
do
  EXCLUDES[i]="--exclude=${EXCLUDE[i]}"
done

#転送元 自身の環境にあわせて変更
FROM="${MYENV}/"
#転送先 環境に合わせて変更
TO="yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html"

#最初は　sshpass -p ${SERVERPASS} rsync -avn --delete "${EXCLUDES[@]}" ${FROM} ${TO} でDRY RUNして確認すること
rsync -e "ssh -p10022 -i /Users/size-nn-mod/.ssh/yogaosaka.key" -av --delete "${EXCLUDES[@]}" ${FROM} ${TO}
