#!/bin/sh
#【注意】ssh接続できるのが前提です。
#sshpass用のパスワード

MYENV='/Applications/MAMP/htdocs/yoga'
REMOTEDEFAULT='yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html'
#除外ファイルを指定して纏める 転送元をトップとして絶対パス指定
#リモートでcongif設定変更をする場合は除外ファイルに "config/generated_overrides/" 追加
EXCLUDE=("config/database.php" "**/.DS_Store" "config/doctrine/" "files/" "languages/" "config/generated_overrides/" "themes/**/bower_components/" "themes/**/jade_src/" "themes/**/build/" "themes/**/dist/" "themes/**/node_modules/" "themes/**/*.html" "themes/**/Gruntfile.js" "themes/**/bower.json" "themes/**/package.json" "themes/**/README.md" "themes/**/.*")
# EXCLUDE=("/config/database.php" "**/.DS_Store" "/config/doctrine/" "/languages/" "/suiton_itt_block/.git/"  "themes/**/bower_components/" "themes/**/jade_src/" "themes/**/build/" "themes/**/dist/" "themes/**/node_modules/" "themes/**/*.html" "themes/**/Gruntfile.js" "themes/**/bower.json" "themes/**/package.json" "themes/**/README.md" "themes/**/.*")
for i in "${!EXCLUDE[@]}"
do
  EXCLUDES[i]="--exclude=${EXCLUDE[i]}"
done

#転送元 自身の環境にあわせて変更
FROM="${MYENV}/application/"
#転送先 環境に合わせて変更
TO="yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html/application"

#最初は　sshpass -p ${SERVERPASS} rsync -avn --delete "${EXCLUDES[@]}" ${FROM} ${TO} でDRY RUNして確認すること
rsync -e "ssh -p10022 -i /Users/size-nn-mod/.ssh/yogaosaka.key" -av --delete "${EXCLUDES[@]}" ${FROM} ${TO}

#転送元 自身の環境にあわせて変更
FROM="${MYENV}/packages/"
#転送先 環境に合わせて変更
TO="yogaosaka@sv2229.xserver.jp:/home/yogaosaka/yogadaykansai.com/public_html/packages"

#最初は　rsync -avn --delete "${EXCLUDES[@]}" ${FROM} ${TO} でDRY RUNして確認すること
rsync -e "ssh -p10022 -i /Users/size-nn-mod/.ssh/yogaosaka.key" -av --delete "${EXCLUDES[@]}" ${FROM} ${TO}

# ブロックデザイナーの変更をさせるためにFTPユーザーに権限変更
# ssh -p 2222 yogaosaka@sv2139.xserver.jp "chmod -R 777 ${REMOTEDEFAULT}/application/blocks"

# #画像ディレクトリのパーミッション変更
# ssh -p 2222 yogaosaka@sv2139.xserver.jp "chmod -R 755 ${REMOTEDEFAULT}/application/themes/base_theme/images"