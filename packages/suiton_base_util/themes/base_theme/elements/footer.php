<?php defined('C5_EXECUTE') or die("Access Denied.");?>

		<div class="home-sponsors">
			<section class="contents">
			<h3 class="tC">共 催</h3>
				<?php
					$a = new GlobalArea('共催');
					$a->setBlockLimit(1);
					$a->display();
				?>
			</section>
			<section class="contents">
			<h3 class="tC">協 賛</h3>
			<?php
				$a = new GlobalArea('協賛');
				$a->setBlockLimit(1);
				$a->display();
			?>
			</section>
	<?php if($c->getCollectionID() == 1):?>
			<section class="contents">
			<h3 class="tC">後 援</h3>
			<?php
				$a = new GlobalArea('後援');
				$a->setBlockLimit(1);
				$a->display();
			?>
			</section>
		</div><!-- /home-sponsors -->
	<?php endif;?>
		<div id="responsive_flg"></div>
		<div class="pagetop tC" id="page-top">
			<a href="#pagetop" data-scroll>PAGETOP</a>
		</div><!-- /pagetop -->

	</div><!-- /container -->

	<footer>
		<div class="footer-logo">
		<?php
			$a = new GlobalArea('fooer logo');
			$a->setBlockLimit(1);
			$a->display();
		?>
		</div>
		<?php
			$a = new GlobalArea('copyright');
			$a->setBlockLimit(1);
			$a->display();
		?>
	</footer>
	</div><!-- /wrapper -->

	<script>
		(function($) {
		$(function(){
			smoothScroll.init();
		});
		})(jQuery);
	</script>
</div><!--/#wrapper-->
</div><!--/getPageWrapperClass-->
<?php Loader::element('footer_required'); ?>


<script type='text/javascript' src='//maps.googleapis.com/maps/api/js?key=AIzaSyCBypLqIFOeh5cVnAWhwNIkGIQRX1r-yOY'></script>
<script>
$(document).ready(function($) {
	if($('#map').length == 1){
		/* ===============================================
		mapの初期表示
		=============================================== */
		var currentInfoWindow;
		var markerClusterer = null;
		var map = null;
		var op = {
			zoom:15,
			center:new google.maps.LatLng(35.509961, 135.102149),
			mapTypeId:google.maps.MapTypeId.ROADMAP,
			scrollwheel: false,
			zoomControl:true,
			streetViewControl:false
		};

		map = new google.maps.Map(document.getElementById("map"),op);

		var markers = [];
		var tmp = [];

		var geo = [
			{
				lat:35.0130513,
				lng:135.7809623,
				iconDefault:'<?php echo $this->getThemePath();?>/assets/img/map1.png'
			},
			{
				lat:35.014976,
				lng:135.782470,
				iconDefault:'<?php echo $this->getThemePath();?>/assets/img/map2.png'
			}
		];
		_.each(geo,function(item,index){
			var lat = item.lat;
			var lng = item.lng;
			var marker = {};
			if(lat && lng){
				var obj = {
					position:new google.maps.LatLng(lat,lng),
					map:map,
					//title:item.title
					icon:item.iconDefault
				};

				marker = new google.maps.Marker(obj);

				//markerごと、googlemap上での処理用配列
				markers.push(marker);
				//最後にクラスタに渡す配列
				tmp.push(marker);
			 }
		});
		/* ===============================================
		default position
		=============================================== */
		if (markers.length == 1) {
			map.setCenter(markers[0].getPosition());
			map.setZoom(14);
			var infowindow = new google.maps.InfoWindow({content: inner });
			infowindow.open(map, marker);
		}
		if (markers.length > 0){
			var bounds = new google.maps.LatLngBounds();
			for (var i in markers) {
				// 緯度経度を取得
				if(Object.keys(markers[i]).length !== 0){
					var latlng = markers[i].getPosition();
					// 検索結果地が含まれるように範囲を拡大
					bounds.extend(latlng);
				}
			}
			map.fitBounds(bounds);
		}
	}
});
</script>
</body>
</html>
