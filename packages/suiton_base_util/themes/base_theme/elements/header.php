<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$u = new User();
?>
<!DOCTYPE html><!-- [if IEMobile 7 ]>  <html lang="ja" class="no-js iem7"><![endif] -->
<!-- [if lt IE 7]><html lang="ja" class="no-js lt-ie9 lt-ie8 lt-ie7 ie6"><![endif] -->
<!-- [if IE 7]><html lang="ja" class="no-js lt-ie9 lt-ie8 ie7"><![endif] -->
<!-- [if IE 8]><html lang="ja" class="no-js lt-ie9 ie8"><![endif] -->

<!-- [if (gte IE 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="ja" class="no-js">
<!-- <![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="UTF-8">
	<meta id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,user-scalable=1" name="viewport">
	<?php Loader::element('header_required');?>
	<script>var CCM_THEME_PATH = '<?php echo $this->getThemePath()?>'</script>
	<?php
	//for total pagenation num
	//for ajax pagenation,for infinityscroll etc...
	?>
	<script>var cm_this_pagenation_total = 0;</script>
<?php if($c->isEditMode()):?>
	<style>
		.sp-head,.sp-text,.sp-img, {
			display: block !important ;
		}
		.pc-head,.pc-text,.pc-img  {
			display: inherit !important;
		}

		.sp-head,.pc-text,.sp-text {
			display: block !important ;
		}
		nav#menu:not(.mm-menu){ display: block;}
		.sp-head{
			position: static;
		}
		.fixheader_top ,
		#header h1 ,
		#header.fixed ,
		#header.fixed .fixheader,
		#header .fixheader {
			position: static;
		}
		div#ccm-panel-add-block div.ccm-panel-add-block-set header,
		#header .fixheader,
		#header.fixed .fixheader,
		header {
			height: inherit;
		}
	</style>
<?php endif;?>

<?php
if($u->isLoggedIn()) {
	//ログイン状態で適用したい処理
}
?>
</head>

<body id="pagetop">
<div class="<?php echo $c->getPageWrapperClass();?>">
	<div id="wrapper">
		<div class="sp-head">
			<div class="header Fixed">
				<h1>
					<?php
						$a = new GlobalArea('SP logo');
						$a->setBlockLimit(1);
						$a->display();
					?>
				</h1>
				<div class="menu">
				<a href="#menu"><span class="icon-icon_menu"></span><br>
				MENU</a></div>
			</div>

			<nav id="menu">
				<ul>
					<?php
						$a = new GlobalArea('SP menu');
						$a->display();
					?>
				</ul>
			</nav>
		</div><!-- /sp用header -->

		<header>
			<div class="pc-head">
				<div class="fixheader_top" style="z-index: 10;">
					<div class="btn_en">
						<a href="<?php echo URL::to('/program/english');?>">ENGLISH</a>
					</div>
				</div>
				<div id="header">
					<div class="fixheader">
						<div class="social_btn">
						<ul>
							<li class="icon_f"><a href="https://www.facebook.com/yogadaykansai/" target="_blank"><span class="icon-icon_f"></span></a></li>
							<li class="icon_i"><a href="https://www.instagram.com/yogadaykansai/" target="_blank"><span class="icon-icon_i"></span></a></li>
							<li class="icon_t"><a href="https://twitter.com/yogadaykansai/" target="_blank"><span class="icon-icon_t"></span></a></li>
							<li class="icon_y"><a href="https://www.youtube.com/channel/UCHHJ-b2CBXVDGSwLtiMbbvQ" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>

						</ul>
						</div>
						<nav>
							<h1>
							<?php
								$a = new GlobalArea('PC logo');
								$a->setBlockLimit(1);
								$a->display();
							?>
							</h1>
							<ul>
								<?php
									$a = new GlobalArea('PC menu');
									$a->display();
								?>
							</ul>
						</nav>
					</div>
				</div>
			</div><!-- /pc用header -->

		</header><!-- /header -->

		<div id="container">
