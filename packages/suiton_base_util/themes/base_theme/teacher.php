<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$this->inc('elements/header.php');
//$p= pageobject
$date = $c->getCollectionDatePublic();
$date = date('Y/m/d',strtotime($date));

//$p= pageobject

$main_img = $c->getAttribute('main_img');
$thumbnail = $c->getAttribute('thumbnail');
$data_teacher_link = $c->getAttribute('data_teacher_link');
$data_teacher_affilation = $c->getAttribute('data_teacher_affilation');
$data_teacher_style = $c->getAttribute('data_teacher_style');
$data_teacher_language = $c->getAttribute('data_teacher_language');
$data_teacher_subttl = $c->getAttribute('data_teacher_subttl');
?>
<?php if(is_object($main_img)):?>
	<div class="main_img">
		<img src="<?php echo $main_img->getURL();?>" alt="<?php echo h($c->getCollectionName());?>">
	</div>
<?php endif;?>
<div class="area--main_contents">
	<h1 class="main_ttl--onecol">
		<?php echo h($c->getCollectionName());?>
		<div class="subttl"><?php echo $data_teacher_subttl;?></div>
	</h1>
	<div class="inner--1st">
		<?php
			$a = new Area('メインコンテンツ');
			$a->display($c);
		?>
	</div>

	<div class="inner--2nd">
		<?php if(is_object($thumbnail)):?>
		<div class="thumb_wrap">
			<img src="<?php echo $thumbnail->getURL();?>" alt="">
		</div>
		<?php endif;?>
		<?php if($data_teacher_link):?>
			<div class="inner">
			<?php echo $data_teacher_link;?>
			</div>
		<?php endif;?>

		<?php if($data_teacher_affilation):?>
			<h3 class="subttl">所属/Affiliation</h3>
			<div class="inner">
			<?php echo $data_teacher_affilation;?>
			</div>
		<?php endif;?>

		<?php if($data_teacher_style):?>
			<h3 class="subttl">ヨガスタイル/Yoga Style</h3>
			<div class="inner">
			<?php echo $data_teacher_style;?>
			</div>
		<?php endif;?>

		<?php if($data_teacher_language):?>
			<h3 class="subttl">言語/Language</h3>
			<div class="inner">
			<?php echo $data_teacher_language;?>
			</div>
		<?php endif;?>

		<?php
			$a = new Area('サイドバー');
			$a->display($c);
		?>
	</div>
</div>

<?php
if($c->getCollectionTypeHandle() == 'type_teacher_detail'):?>
<div class="home-staff" style="margin-top:25px;">
	<?php
		$home =  Page::getByID(1);
		$home->outputCustomStyleHeaderItems();
		$content = new Area('講師リスト');
		$content->display($home);
	?>
</div><!-- /home-staff -->
<?php endif; ?>
<?php $this->inc('elements/footer.php') ?>
