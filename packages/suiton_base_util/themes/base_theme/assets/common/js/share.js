/* ======================================================================
  スマホナビ用
====================================================================== */

if(!CCM_EDIT_MODE){
	$(function() {
		$("#menu").mmenu({
	         "extensions": [
	            "pagedim-black"
	         ]
	     });
	});
}

/* ======================================================================
  ヘッダーナビ用
====================================================================== */
$(window).on('scroll', function() {
    $('#header').toggleClass('fixed', $(this).scrollTop() > 40);
});


/* ======================================================================
  ページトップボタン用
====================================================================== */
$(function() {
	var showFlug = false;
	var topBtn = $('#page-top');
	topBtn.css('bottom', '-100px');
	var showFlug = false;
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			if (showFlug == false) {
				showFlug = true;
				topBtn.stop().animate({'bottom' : '30px'}, 200);
			}
		} else {
			if (showFlug) {
				showFlug = false;
				topBtn.stop().animate({'bottom' : '-100px'}, 200);
			}
		}
	});
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
});

/* navi
----------------------- */
(function($) {
$(function(){
	$('li.has_child > a').each(function(){
		$(this).on('click',function(){
			var $child = $($(this).next('ul'));

			if($child.css('display') == 'block'){
				$child.slideUp('200');
			}else{
				$child.slideDown('200');
			}

			return false;
		});
	});
});
})(jQuery);

/* sidebar
----------------------- */
(function($) {
$(function(){
	$('.list--news--side , .swiper-container--news').find('li').on('click',function(){
		var link = $(this).find('a').first().attr('href');
		window.location = link;
	});
});
})(jQuery);


(function($) {
$(function(){
	$(window).on('load',function(){
		$('.row.list--program , .home-news .swiper-wrapper.row , .home-sponsors .row' ).fixHeight();
	});
});
})(jQuery);
