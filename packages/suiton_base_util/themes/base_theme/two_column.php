<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$this->inc('elements/header.php');
//$p= pageobject
$date = $c->getCollectionDatePublic();
$date = date('Y/m/d',strtotime($date));
?>
<div class="area--main_contents">
	<div class="inner--1st">
		<h2 class="main_ttl--onecol"><?php echo h($c->getCollectionName());?></h2>
		<time class="time"><?php echo $date;?></time>
		<?php
			$a = new Area('メインコンテンツ');
			$a->display($c);
		?>
		<?php
			$a = new GlobalArea('ニュースフッター');
			$a->display();
		?>
	</div>

	<div class="inner--2nd">
		<?php
			$a = new GlobalArea('サイドバー');
			$a->display();
		?>
	</div>
</div>

<?php $this->inc('elements/footer.php') ?>
