<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$this->inc('elements/header.php');
//$p= pageobject
$thumbnail = $c->getAttribute('thumbnail');
?>
<div class="area--main_contents">
	<?php if($thumbnail):?>
	<div class="ac lower_mv">
		<img src="<?php echo $thumbnail->getURL();?>" alt="<?php echo h($c->getCollectionName());?>">
	</div>
	<?php endif;?>
	<div class="inner--1st">
		<h2 class="main_ttl--onecol"><?php echo h($c->getCollectionName());?></h2>
		<?php
			$a = new Area('メインコンテンツ');
			$a->setCustomTemplate('image', 'fullwidth_parallax.php');
			$a->display($c);
		?>
	</div>
</div>

<?php $this->inc('elements/footer.php') ?>
