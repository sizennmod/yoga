<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$this->inc('elements/header.php');
?>

<div class="home-img" style="background: none;">
	<?php
		$a = new Area('Main Visual');
		$a->display($c)
	?>
	<!-- Swiper JS -->
	<!-- Initialize Swiper -->
</div><!-- /home-img -->

<div class="home-catch">
	<section class="contents tC">
			<?php
				$a = new Area('タイトル');
				$a->display($c)
			?>
			<div class="pc-text">
				<?php
					$a = new Area('PC用リード文');
					$a->display($c)
				?>
			</div>
			<div class="sp-text tL">
				<?php
					$a = new Area('SP用リード文');
					$a->display($c)
				?>
			</div>
	</section>
</div><!-- /home-catch -->

<div class="home-news">
<div class="home-news__news-h3">
<section class="contents">
	<?php
		$a = new Area('ニュースタイトル+リンク');
		$a->display($c)
	?>
</section>
</div>
<section class="contents">
<?php
	$a = new Area('ニュースリスト');
	$a->display($c)
?>
</section>
</div><!-- /home-news -->


<div class="home-about">
<?php
	$a = new Area('概要エリア メインビジュアル');
	$a->display($c)
?>
<section class="contents home-about__about-text">
<div class="pc-text">
	<?php
		$a = new Area('概要エリア PC用テキスト');
		$a->display($c)
	?>

</div>
<div class="sp-text">
	<?php
		$a = new Area('概要エリア SP用テキスト');
		$a->display($c)
	?>
</div>
<br>
<?php
	$a = new Area('概要エリア リンクボタン');
	$a->display($c)
?>
</section>
</div><!-- /home-about -->

<div class="home-event">
<section class="contents tC">
	<?php
		$a = new Area('概要エリア２ メインビジュアル');
		$a->display($c)
	?>
	<?php
		$a = new Area('概要エリア２ タイトル');
		$a->display($c)
	?>
	<div class="pc-text">
		<?php
			$a = new Area('概要エリア２ PC用テキスト');
			$a->display($c)
		?>
	</div>
	<div class="sp-text tL">
		<?php
			$a = new Area('概要エリア２ SP用テキスト');
			$a->display($c)
		?>
	</div>
	<br>
	<?php
		$a = new Area('概要エリア２ リンクボタン');
		$a->display($c)
	?>
</section>
</div><!-- /home-event -->

<div class="home-eventBtn">
<?php
	$a = new Area('画像リンクエリア1');
	$a->display($c)
?>
</div><!-- /home-event -->
<div class="home-ticket">
<section class="contents">
<div class="home-ticket__block">
	<div class="pc-text">
		<?php
			$a = new Area('チケットエリア１ PC用テキスト');
			$a->display($c)
		?>
	</div>
	<div class="sp-text">
		<?php
			$a = new Area('チケットエリア１ SP用テキスト');
			$a->display($c)
		?>
	</div>
	<?php
		$a = new Area('チケットエリア１ リンクボタン');
		$a->display($c)
	?>
</div>
</section>
</div><!-- /home-ticket -->

<div class="home-staff">
	<?php
		$a = new Area('講師リスト');
		$a->display($c)
	?>
</div><!-- /home-staff -->

<div class="home-map">
<div id="map"></div>
</div><!-- /home-map -->
<?php
	// $a = new Area('メインコンテンツ');
	// $a->display($c);
?>
<?php $this->inc('elements/footer.php') ?>
