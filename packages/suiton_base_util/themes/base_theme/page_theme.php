<?php
namespace Concrete\Package\SuitonBaseUtil\Theme\BaseTheme;
defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
use Concrete\Core\Page\Theme\Theme;

class PageTheme extends Theme implements ThemeProviderInterface
{
	/**
	 * テーマで利用するアセットファイル群の定義
	 */
	public function registerAssets() {
		$this->requireAsset('javascript', 'jquery');
		$this->requireAsset('javascript', 'underscore');
		$this->requireAsset('javascript', 'jquery/ui');
		$this->requireAsset('css', 'jquery/ui');

		/* my css
		package/controllerで定義したアセットハンドルを配列に記述
		----------------------- */
		$local_css = array(
			'Julius-css',
			'swiper-css',
			'mmenu-css',
			'style-min-css',
		);
		foreach($local_css as $css){
			$this->requireAsset('css', $css);
		}

		/* my js
		package/controllerで定義したアセットハンドルを配列に記述
		----------------------- */
		$local_js = array(
			'fixheight-js',
			'mmenu-js',
			'pagescroll-js',
			'share-js',
			'ga-js',
			'swiper-js',
			'awesome-js'
		);
		foreach($local_js as $js){
			$this->requireAsset('javascript', $js);
		}
	}
	/**
	 * エリアのカスタムクラス設定時にデフォルト登録しておくクラス
	 *
	 * @return array
	 */
	public function getThemeAreaClasses()
	{
		return array(
			// 'Main' => array(
			// 	'mt80'
			// )
		);
	}
	/**
	 * ブロックのカスタムクラス設定時にデフォルト登録しておくクラス
	 *
	 * @return array
	 */
	public function getThemeBlockClasses()
	{
		return array(
			// 'core_area_layout' => array(
			// 	'mt80'
			// )
			'image' => array(
				'lower_mv',
				'ac'
			),
			'content' => array(
				'w640'
			),
			'youtube' => array(
				'w640'
			),
			'ex_title' => array(
				'gold','maxw960'
			),
		);
	}
	/**
	 * エディタのカスタムスタイル設定時にデフォルト登録しておくクラス
	 *
	 * @return array
	 */
	public function getThemeEditorClasses()
	{
		return array(
			// array(
			// 	'title' => t('lead'),
			// 	'menuClass' => '',
			// 	'spanClass' => 'lead',
			// 	'forceBlock' => 1//ブロック要素で囲む
			// ),
			// array(
			// 	'title' => t('text-color-gold'),
			// 	'menuClass' => '',
			// 	'spanClass' => 'color-gold',
			// 	'forceBlock' => -1//spanで囲む
			// )
			array(
				'title' => t('text34'),
				'menuClass' => '',
				'spanClass' => 'text34',
				'forceBlock' => -1//spanで囲む
			),
			array(
				'title' => t('メインタイトル'),
				'menuClass' => '',
				'spanClass' => 'main_ttl--onecol',
				'forceBlock' => 1//ブロック要素で囲む
			)
		);
	}
	/**
	 * カスタムレイアウトプリセット
	 * https://concrete5-japan.org/help/5-7/developer/designing-for-concrete5/adding-complex-custom-layout-presets-in-your-theme/
	 * @return array
	 */
	public function getThemeAreaLayoutPresets()
	{
		$presets = [
			[
				'handle' => 'bg_a',
				'name' => 'グレー背景エリア',
				'container' => '<div class="block-bgA"></div>',
				'columns' => [
					'<section class="contents"></section>'
				],
			],
			[
				'handle' => 'bg_b',
				'name' => '白背景エリア',
				'container' => '<div class="block-bgB"></div>',
				'columns' => [
					'<section class="contents"></section>'
				],
			]
		];

		return $presets;
	}
}
